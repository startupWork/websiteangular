// import rollup      from 'rollup'
import nodeResolve from 'rollup-plugin-node-resolve'
import commonjs    from 'rollup-plugin-commonjs';
// import uglify      from 'rollup-plugin-uglify'

//paths are relative to the execution path
export default {
  input: 'src/main-aot.js',
  output : {
      file : 'aot/dist/build.js', // output a single application bundle
      format: 'iife'
    },
  onwarn: function(warning) {
    // Skip certain warnings

    // should intercept ... but doesn't in some rollup versions
    if ( warning.code === 'THIS_IS_UNDEFINED' ) { return; }

    // console.warn everything else
    console.warn( warning.message );
  },
  plugins: [
    nodeResolve({jsnext: true, module: true}),
    commonjs({
        // include: 'node_modules/rxjs/**',
        include: ['node_modules/**'],
        namedExports: {
          'node_modules/fast-levenshtein/levenshtein.js': [ 'get' ],
          'node_modules/elasticsearch-browser/elasticsearch.js': [ 'Client' ],
          'node_modules/d3-cloud/index.js' : ['size']
        }
      }),
      // uglify(),
    // commonjs({
    //   include: ['node_modules/**'],
    //   namedExports: {
  //   // left-hand side can be an absolute path, a path
  //   // relative to the current directory, or the name
  //   // of a module in node_modules
    // 'node_modules/elasticsearch-browser/elasticsearch.js': [ 'Client' ],
    // 'node_modules/fast-levenshtein/levenshtein.js': [ 'get' ],
    // 'node_modules/d3-cloud/index.js' : ['size']
    // 'node_modules/d3-dispatch/index.js' : ['default'],
    // 'node_modules/d3-cloud/index.js' : ['cloud', 'size']
  // }
  //   })
  ]
}
