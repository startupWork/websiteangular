module.exports = function(config) {
   config.set({

       basePath: '',

       frameworks: ["jasmine", "karma-typescript"],

       files: [

        //    { pattern : './src/app/affiliations/affiliation-display/affiliations.spec.ts' },
        //    { pattern : './src/app/affiliations/affiliation-display/affiliation.ts' },
        //    { pattern : './node_modules/zone.js/dist/zone.js'},
        //    { pattern : './node_modules/**/*'},
        //    { pattern : './src/app/services/es-lab-serv.ts'},
        //     { pattern: "src/systemjs-angular-loader.js" },
        //    { pattern: "src/systemjs.config.js" },
        // { pattern: "src/main.ts" },
        //    { pattern: "src/app/*.+(ts|html)" },
        //    { pattern: "src/app/**/*.+(ts|html)" }
       ],

       preprocessors: {
           "**/*.ts": ["karma-typescript"]
       },

       karmaTypescriptConfig: {
           bundlerOptions: {
               entrypoints: /\.spec\.ts$/,
               transforms: [
                   require("karma-typescript-angular2-transform")
               ]
           },
           compilerOptions: {
               lib: ["es2016", "dom"]
           }
       },

       reporters: ["dots", "karma-typescript"],

       browsers: ['Firefox'],
       //
    //    autoWatch: true,
       //
       plugins: [
           'karma-firefox-launcher',
           "karma-typescript",
           "jasmine",
        //    'karma-browserify',
           'karma-jasmine',
        //    'karma-junit-reporter',
        //    'karma-systemjs'
       ]

   });
};
