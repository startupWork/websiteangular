import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';


import { UsersAffKeywords } from './users-aff-keywords';

@NgModule({
  imports: [RouterModule.forChild([
    { path: ':id/:keywords', component: UsersAffKeywords},
  ])],
  exports: [RouterModule]
})
export class UsersAffKeywordsRoutingModule {}
