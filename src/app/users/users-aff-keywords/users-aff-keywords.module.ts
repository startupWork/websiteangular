import { NgModule }  from '@angular/core';
import { CommonModule }  from '@angular/common';
import { SharedModule } from '../../shared.module';
import {  UserSharedModule} from '../userShared.module';
import {UsersAffKeywordsRoutingModule } from './users-aff-keywords-routing.module';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";

import { UsersAffKeywords } from './users-aff-keywords';

@NgModule({
  imports: [CommonModule, SharedModule, UserSharedModule, UsersAffKeywordsRoutingModule, MaterialModule, FormsModule, FlexLayoutModule],
  declarations: [UsersAffKeywords]
})

export class UsersAffKeywordsModule {}
