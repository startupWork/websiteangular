import {Component, Input, OnChanges, SimpleChange, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {MatDialog} from '@angular/material';
import {SearchIndivService} from '../../services/es-indiv-serv';

/*
*    Component to display individuals in list
*/
@Component({
    selector : 'users-search',
    templateUrl : './users-search.html'
})

/**
 * Controller search user by competencies
 */
 export class UsersSearch implements OnInit{

     //text enter by user
     @Input() textSearchIn : string;
     @Input() exactMatch : boolean;
     @Input() isaPerson : boolean;

    // Variable initialization
    explanationBtn : boolean = false;
    isSearchUser : boolean = true;
    isBigram : boolean = false;
    searchResp;
    encodedSearchText : string;

    searchText;
    currentPage;

    //Use for results info
    pageSize = {count: 30, label: "30 per page"};
    resultStat;
    numPages : number;

    researchStart = false;
    researchProcess = false;

    private totalItems = 0;

    constructor(private searchIndivService : SearchIndivService){
    }

    ngOnInit(){
        this.textSearch(this.textSearchIn, 1)
    }

    // Functionality
    /**
     * Select the page to display
     * @param page : number of the page to display
     */
    selectPage(page) {
        this.textSearch(this.searchText, page.numPage);
    }

    /**
     *textSearch : Use to get response of the search like
     * @param text : text use to get the user
     * @param page : number of the page to display
     */
    textSearch(text : string, page : number) {
            this.searchText = text;
            //Use to get the publication of fthe user by keywords
            this.researchProcess = true;
            this.encodedSearchText = text;
            this.currentPage = page;
            this.researchStart = false;
            this.isBigram = false;
            this.explanationBtn = false;
            let from = (this.currentPage - 1) * this.pageSize.count;
            // Get all the user who match with the search
            if(this.exactMatch){
                let name = text.toLowerCase().trim();
                name = name.replace(/é/g, "e");
                name = name.replace(/è/g, "e");
                name = name.replace(/ê/g, "e");
                name = name.replace(/ë/g, "e");
                name = name.replace(/à/g, "a");
                name = name.replace(/ï/g, "i");
                name = name.replace(/î/g, "i");
                name = name.replace(/ô/g, "o");
                name = name.replace(/ç/g, "c");
                name = name.replace(/ü/g, "u");
                name = name.replace(/ù/g, "u");
                name = name.replace(/-/g, " ");
                this.searchIndivService.getUserExactMatch(name).then(
                    resp => {
                        this.researchStart = false;
                        this.researchProcess = false;
                        resp.hits.hits[0].inner_hits = {"pubs" : {"hits" : {"total" : undefined}}};
                        this.searchResp = resp;
                        this.totalItems = resp.hits.total;
                        this.explanationBtn = true;
                        this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                        this.resultStat = {total : resp.hits.total, took : resp.took};
                    }
                )
            }else if(this.isaPerson) {
                this.searchIndivService.getUserMatch(this.searchText).then(
                    resp => {
                        this.researchStart = false;
                        this.researchProcess = false;
                        for(let i=0; i<resp.hits.hits.length; i++){
                            resp.hits.hits[i].inner_hits = {"pubs" : {"hits" : {"total" : undefined}}};
                        }
                        this.searchResp = resp;
                        this.totalItems = resp.hits.total;
                        this.explanationBtn = true;
                        this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                        this.resultStat = {total : resp.hits.total, took : resp.took};
                    }
                )
            }else if(text.indexOf('"') != -1 && text.indexOf('"') != text.lastIndexOf('"') && text.split(" ").length > 1) {
                let re = /\"(.*?)\"/g;
                let newtext = text.match(re);
                let unigrams = text;
                let tri = "";
                let bi = "";
                for(let i = 0; i < newtext.length; i++){
                    let biortri = newtext[i];
                    unigrams = unigrams.replace(biortri, '');
                    biortri = biortri.replace('"', '');
                    biortri = biortri.replace('"', '');
                    let biortri_split = newtext[i].split(" ");
                    biortri = biortri.replace('"', '');
                    biortri = biortri.replace('"', '');
                    if(biortri_split.length == 2){
                        bi = bi +" "+ biortri
                    }
                    if(biortri_split.length == 3){
                        tri = tri +" "+ biortri
                    }
                    if(biortri_split.length > 3){
                        unigrams = unigrams + biortri
                    }
                }
                // text = text.replace('"', '');
                // text = text.replace('"', '');
                this.isBigram = true;
                if(bi != "" && tri != "" && unigrams.match(/[A-Za-z0-9àâçéèêëîïôûùüÿñæœ]+/g)){
                    this.searchIndivService.getPublicationsByUserBigramsTrigramsText(from, this.pageSize.count, tri, bi, unigrams).then(
                        resp => {
                           if (resp.hits.total == 0) {
                               this.isBigram = false;
                               this.searchIndivService.userTextSearchInRange(from, this.pageSize.count, this.searchText).then(
                                    resp => {
                                       if(resp.hits.total == 0){
                                           this.researchStart = true;
                                           this.researchProcess = false;
                                           this.searchResp = undefined;
                                       }else{
                                           this.researchStart = false;
                                           this.researchProcess = false;
                                           this.searchResp = resp;
                                           this.totalItems = resp.hits.total;
                                           this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                                           this.resultStat = {total : resp.hits.total, took : resp.took};
                                       }
                                   });
                           } else {
                               this.researchStart = false;
                               this.researchProcess = false;
                               this.searchResp = resp;
                               this.totalItems = resp.hits.total;
                               this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                               this.resultStat = {total: resp.hits.total, took: resp.took};
                           }
                       });
                }else if(bi != "" && tri != "" && !unigrams.match(/[A-Za-z0-9àâçéèêëîïôûùüÿñæœ]+/g)){
                    this.searchIndivService.getPublicationsByUserBigramsTrigrams(from, this.pageSize.count, tri, bi).then(
                        resp => {
                           if (resp.hits.total == 0) {
                               this.isBigram = false;
                               this.searchIndivService.userTextSearchInRange(from, this.pageSize.count, this.searchText).then(
                                    resp => {
                                       if(resp.hits.total == 0){
                                           this.researchStart = true;
                                           this.researchProcess = false;
                                           this.searchResp = undefined;
                                       }else{
                                           this.researchStart = false;
                                           this.researchProcess = false;
                                           this.searchResp = resp;
                                           this.totalItems = resp.hits.total;
                                           this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                                           this.resultStat = {total : resp.hits.total, took : resp.took};
                                       }
                                   });
                           } else {
                               this.researchStart = false;
                               this.researchProcess = false;
                               this.searchResp = resp;
                               this.totalItems = resp.hits.total;
                               this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                               this.resultStat = {total: resp.hits.total, took: resp.took};
                           }
                       });
                }else if(bi == "" && tri != "" && unigrams.match(/[A-Za-z0-9àâçéèêëîïôûùüÿñæœ]+/g)){
                    this.searchIndivService.getPublicationsByUserTrigramsText(from, this.pageSize.count, tri, unigrams).then(
                        resp => {
                           if (resp.hits.total == 0) {
                               this.isBigram = false;
                               this.searchIndivService.userTextSearchInRange(from, this.pageSize.count, this.searchText).then(
                                    resp => {
                                       if(resp.hits.total == 0){
                                           this.researchStart = true;
                                           this.researchProcess = false;
                                           this.searchResp = undefined;
                                       }else{
                                           this.researchStart = false;
                                           this.researchProcess = false;
                                           this.searchResp = resp;
                                           this.totalItems = resp.hits.total;
                                           this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                                           this.resultStat = {total : resp.hits.total, took : resp.took};
                                       }
                                   });
                           } else {
                               this.researchStart = false;
                               this.researchProcess = false;
                               this.searchResp = resp;
                               this.totalItems = resp.hits.total;
                               this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                               this.resultStat = {total: resp.hits.total, took: resp.took};
                           }
                       });
                }else if(bi != "" && tri == "" && unigrams.match(/[A-Za-z0-9àâçéèêëîïôûùüÿñæœ]+/g)){
                    this.searchIndivService.getPublicationsByUserBigramsText(from, this.pageSize.count, bi, unigrams).then(
                        resp => {
                           if (resp.hits.total == 0) {
                               this.isBigram = false;
                               this.searchIndivService.userTextSearchInRange(from, this.pageSize.count, this.searchText).then(
                                    resp => {
                                       if(resp.hits.total == 0){
                                           this.researchStart = true;
                                           this.researchProcess = false;
                                           this.searchResp = undefined;
                                       }else{
                                           this.researchStart = false;
                                           this.researchProcess = false;
                                           this.searchResp = resp;
                                           this.totalItems = resp.hits.total;
                                           this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                                           this.resultStat = {total : resp.hits.total, took : resp.took};
                                       }
                                   });
                           } else {
                               this.researchStart = false;
                               this.researchProcess = false;
                               this.searchResp = resp;
                               this.totalItems = resp.hits.total;
                               this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                               this.resultStat = {total: resp.hits.total, took: resp.took};
                           }
                       });
                }else if(bi != "" && tri == "" && !unigrams.match(/[A-Za-z0-9àâçéèêëîïôûùüÿñæœ]+/g)){
                    this.searchIndivService.userTextSearchInRangeBigrams(from, this.pageSize.count, bi).then(
                        resp => {
                           if (resp.hits.total == 0) {
                               this.isBigram = false;
                               this.searchIndivService.userTextSearchInRange(from, this.pageSize.count, this.searchText).then(
                                    resp => {
                                       if(resp.hits.total == 0){
                                           this.researchStart = true;
                                           this.researchProcess = false;
                                           this.searchResp = undefined;
                                       }else{
                                           this.researchStart = false;
                                           this.researchProcess = false;
                                           this.searchResp = resp;
                                           this.totalItems = resp.hits.total;
                                           this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                                           this.resultStat = {total : resp.hits.total, took : resp.took};
                                       }
                                   });
                           } else {
                               this.researchStart = false;
                               this.researchProcess = false;
                               this.searchResp = resp;
                               this.totalItems = resp.hits.total;
                               this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                               this.resultStat = {total: resp.hits.total, took: resp.took};
                           }
                       });
                }else if(bi == "" && tri != "" && !unigrams.match(/[A-Za-z0-9àâçéèêëîïôûùüÿñæœ]+/g)){
                    this.searchIndivService.userTextSearchInRangeTrigrams(from, this.pageSize.count, tri).then(
                        resp => {
                           if (resp.hits.total == 0) {
                               this.isBigram = false;
                               this.searchIndivService.userTextSearchInRange(from, this.pageSize.count, this.searchText).then(
                                    resp => {
                                       if(resp.hits.total == 0){
                                           this.researchStart = true;
                                           this.researchProcess = false;
                                           this.searchResp = undefined;
                                       }else{
                                           this.researchStart = false;
                                           this.researchProcess = false;
                                           this.searchResp = resp;
                                           this.totalItems = resp.hits.total;
                                           this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                                           this.resultStat = {total : resp.hits.total, took : resp.took};
                                       }
                                   });
                           } else {
                               this.researchStart = false;
                               this.researchProcess = false;
                               this.searchResp = resp;
                               this.totalItems = resp.hits.total;
                               this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                               this.resultStat = {total: resp.hits.total, took: resp.took};
                           }
                       });
                }else if(bi == "" && tri == "" && unigrams.match(/[A-Za-z0-9àâçéèêëîïôûùüÿñæœ]+/g)){
                    this.searchIndivService.userTextSearchInRange(from, this.pageSize.count, unigrams).then(
                         resp => {
                            if(resp.hits.total == 0){
                                this.researchStart = true;
                                this.researchProcess = false;
                                this.searchResp = undefined;
                            }else{
                                this.researchStart = false;
                                this.researchProcess = false;
                                this.searchResp = resp;
                                this.totalItems = resp.hits.total;
                                this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                                this.resultStat = {total : resp.hits.total, took : resp.took};
                            }
                        });
                }
                // text = text.replace('"', '');
                // text = text.replace('"', '');
                // this.isBigram = true;
                // if(text.split(" ").length == 2){
                //         this.searchIndivService.userTextSearchInRangeBigrams(from, this.pageSize.count, text).then(
                //              resp => {
                //                 if (resp.hits.total == 0) {
                //                     this.isBigram = false;
                //                     this.searchIndivService.userTextSearchInRange(from, this.pageSize.count, this.searchText).then(
                //                          resp => {
                //                             if(resp.hits.total == 0){
                //                                 this.researchStart = true;
                //                                 this.researchProcess = false;
                //                                 this.searchResp = undefined;
                //                             }else{
                //                                 this.researchStart = false;
                //                                 this.researchProcess = false;
                //                                 this.searchResp = resp;
                //                                 this.totalItems = resp.hits.total;
                //                                 this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                //                                 this.resultStat = {total : resp.hits.total, took : resp.took};
                //                             }
                //                         });
                //                 } else {
                //                     this.researchStart = false;
                //                     this.researchProcess = false;
                //                     this.searchResp = resp;
                //                     this.totalItems = resp.hits.total;
                //                     this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                //                     this.resultStat = {total: resp.hits.total, took: resp.took};
                //                 }
                //             });
                //     }else{
                //         this.searchIndivService.userTextSearchInRangeTrigrams(from, this.pageSize.count, text).then(
                            //  resp => {
                            //     if (resp.hits.total == 0) {
                            //         this.isBigram = false;
                            //         this.searchIndivService.userTextSearchInRange(from, this.pageSize.count, this.searchText).then(
                            //              resp => {
                            //                 if(resp.hits.total == 0){
                            //                     this.researchStart = true;
                            //                     this.researchProcess = false;
                            //                     this.searchResp = undefined;
                            //                 }else{
                            //                     this.researchStart = false;
                            //                     this.researchProcess = false;
                            //                     this.searchResp = resp;
                            //                     this.totalItems = resp.hits.total;
                            //                     this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                            //                     this.resultStat = {total : resp.hits.total, took : resp.took};
                            //                 }
                            //             });
                            //     } else {
                            //         this.researchStart = false;
                            //         this.researchProcess = false;
                            //         this.searchResp = resp;
                            //         this.totalItems = resp.hits.total;
                            //         this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                            //         this.resultStat = {total: resp.hits.total, took: resp.took};
                            //     }
                            // });
                //     }
            }else{
                this.searchIndivService.userTextSearchInRange(from, this.pageSize.count, text).then(
                     resp => {
                        if(resp.hits.total == 0){
                            this.researchStart = true;
                            this.researchProcess = false;
                            this.searchResp = undefined;
                        }else{
                            this.researchStart = false;
                            this.researchProcess = false;
                            this.searchResp = resp;
                            this.totalItems = resp.hits.total;
                            this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                            this.resultStat = {total : resp.hits.total, took : resp.took};
                        }
                    });
            }
    }

    /**
     *  isAvailableResults : check if results
     *  @return {boolean}
     *
     */
    isAvailableResults() {
        return this.searchResp ? true : false;
    }

    /**
     * isAtLeastOneResult : check if at least one result
     * @returns {boolean}
     */
    isAtLeastOneResult() {
        if (!this.isAvailableResults()) {
            return false;
        }
        return this.searchResp.hits.total > 0;
    }

    /**
     * Use to check if is a mobile or not to display list instead of cards
     * @returns {boolean} : true if is a mobile
     */
    mobileCheck() {
        let check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor);
        return check;
    }

}
