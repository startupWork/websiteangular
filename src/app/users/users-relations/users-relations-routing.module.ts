import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';


import { UsersRelations } from './users-relations';

@NgModule({
  imports: [RouterModule.forChild([
    { path: ':userId', component: UsersRelations},
  ])],
  exports: [RouterModule]
})
export class UserRelationRoutingModule {}
