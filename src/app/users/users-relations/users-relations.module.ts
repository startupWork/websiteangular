import { NgModule }  from '@angular/core';
import { CommonModule }  from '@angular/common';
import { SharedModule } from '../../shared.module';
import {  UserSharedModule} from '../userShared.module';
import {UserRelationRoutingModule } from './users-relations-routing.module';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";

import { UsersRelations } from './users-relations';
import { SocialGraph } from '../../d3_template/social-graph/social-graph';

@NgModule({
  imports: [CommonModule, SharedModule, UserSharedModule, UserRelationRoutingModule, MaterialModule, FormsModule, FlexLayoutModule],
  declarations: [UsersRelations, SocialGraph]
})

export class UserRelationModule {}
