import {Component, Input, OnChanges, SimpleChange} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {MatDialog} from '@angular/material';
import {SearchIndivService} from '../../services/es-indiv-serv';
import {ActivatedRoute} from "@angular/router";
import {UserUtils} from '../../utils/user.utils';
import {CleanWords} from '../../utils/cleanwords.utils';
import {DialogWordsCloud} from '../../template-page/dialog-words-cloud/dialog-words-cloud';


/**
 * Module use to control the user relation with social graph
 */

 @Component({
     selector : 'users-relations',
     templateUrl : './users-relations.html'
 })

export class UsersRelations{

    // Variable initialization

    firstName = "";
    lastName = "";


    pageSize = {count: 10, label: "10 per page"};
    currentPage = 1;
    words = [];
    resultStat;
    fullFormatName = "";
    totalItems;
    userUtils = new UserUtils();
    cleanWords = new CleanWords();
    searchResp;
    origin;
    numPages;
    done = false;
    nb_pubs = 0;
    private id;

    constructor(private route: ActivatedRoute, private dialog: MatDialog, private searchIndivService : SearchIndivService){
        this.route.params.subscribe(params => {
            // Récupération des valeurs de l'URL
            this.id = params['userId'];
        });
        this.init_user_key();
    }

    init_user_key(){
          this.searchIndivService.getUser(this.id).then(
              (resp)=> {
                  this.firstName = resp._source.forename;
                  this.lastName = resp._source.surname;
                  this.nb_pubs = resp._source.pubs.length;
                  this.origin = {
                      firstName: resp._source.forename,
                      lastName: resp._source.surname,
                      userId: resp._id
                  };
                  this.getSimilarUsers(10, this.id);
              }
          );
    }

    /**
     * Get similar user of the user select
     * @param size : number of user to show
     * @param userId : id of the user
     */
     getSimilarUsers(size, userId) {
       this.done = false;
        let from = (this.currentPage - 1) * this.pageSize.count;
        // if(this.nb_pubs > 10){
            this.searchIndivService.getSimilarUsers(from, size, userId).then(
                (resp) => {
                    this.searchResp = resp;
                    this.totalItems = resp.hits.total;
                    this.resultStat = {total : resp.hits.total, took : resp.took};
                    this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
                    this.done = true;
                }
            )
        // }else{
        //     this.searchIndivService.getSimilarUsers10(from, size, userId).then(
        //         (resp) => {
        //             this.searchResp = resp;
        //             this.totalItems = resp.hits.total;
        //             this.resultStat = {total : resp.hits.total, took : resp.took};
        //             this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
        //             this.done = true;
        //         }
        //     )
        // }
    };

    /**
     * Return the page select by the user
     * @param page : page select
     */
    selectPage(page) {
        this.currentPage = page.numPage;
        this.getSimilarUsers(this.pageSize.count, this.id);
    };

    /**
     * Select the new user with new select page size option
     * @param pageSize : nb user to display by page
     */
    selectPageSize(pageSize){
        if(this.currentPage * pageSize.pageSize.count > this.totalItems){
            this.currentPage = Math.ceil(this.totalItems / pageSize.pageSize.count)
        }
        this.pageSize = pageSize.pageSize;
        this.getSimilarUsers(this.pageSize.count, this.id);
    };

    /**
     * Check if some result are available
     * @returns {boolean} : true if some result
     */
    isAvailableResults() {
        return this.searchResp ? true : false;
    };

    /**
     * Check if at least oen result is find
     * @returns {boolean} : true if one result is find
     */
    isAtLeastOneResult() {
        if (!this.isAvailableResults()) {
            return false;
        }
        return this.searchResp.hits.total > 0;
    };

    /**
     * Go the explanation about the user and show the dialog
     * @param userId : id of the user
     * @param firstName : first name of the user
     * @param lastName : last name of the user
     */
    showCloud(userId, firstName, lastName) {
            let words = this.formattedUserSearchExplain(userId);
            let bigramExplain = [];
            for(let i = 0; i < words.length; i++){
                let isbigram = words[i].text.split(" ");
                if(isbigram[1] != undefined){
                    bigramExplain.push(words[i].text);
                    words.splice(i, 1);
                }
            }
            let msg = "Explanation for " + firstName + " " + lastName;
        this.openDialog(words, msg, bigramExplain);

    };
    /**
     * Use the word to format them for display the in the word cloud with different size (importance)
     * @param userId : user id
     * @returns {Array} : list of words format
     */
    formattedUserSearchExplain(userId) {
            let explainJson = this.userUtils.affSearchExplain(userId, this.searchResp);
            let resultwordtext = this.cleanWords.getCleanLevenshtein(explainJson);
            return this.userUtils.normalizeScore(resultwordtext);
    }

    /**
     * Open a md Dialog with word cloud
     * @param bigrams : bigrams to show
     * @param msg : message to show one to of the dialog
     * @param words : words to show in the dialog
     */
    openDialog(words, msg, bigrams){
            let dial = this.dialog.open(DialogWordsCloud, {data : {words : words, msg : msg, bigrams :bigrams}});
    }


    /**
     * Use to check if is a mobile or not to display list instead of cards
     * @returns {boolean} : true if is a mobile
     */
    mobileCheck() {
        var check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor);
        return check;
    };
}
