import { NgModule }  from '@angular/core';
import { CommonModule }  from '@angular/common';
import { SharedModule } from '../shared.module';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";

import {SearchIndivService} from '../services/es-indiv-serv';

@NgModule({
  imports: [CommonModule, SharedModule, MaterialModule, FormsModule, FlexLayoutModule],
  declarations: [ ],
  providers : [ SearchIndivService],
})

export class UserSharedModule {}
