import { NgModule }  from '@angular/core';
import { CommonModule }  from '@angular/common';
import { SharedModule } from '../../shared.module';
import {  UserSharedModule} from '../userShared.module';
import {UserSimilarRoutingModule } from './user-similars-routing.module';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";


import { UsersSimilar } from './user-similar';

@NgModule({
  imports: [CommonModule, SharedModule, UserSharedModule, UserSimilarRoutingModule, MaterialModule, FormsModule, FlexLayoutModule],
  declarations: [UsersSimilar]
})

export class UserSimilarModule {}
