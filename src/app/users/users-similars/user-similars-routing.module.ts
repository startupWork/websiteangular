import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';


import { UsersSimilar } from './/user-similar';

@NgModule({
  imports: [RouterModule.forChild([
    { path: ':userId', component: UsersSimilar},
  ])],
  exports: [RouterModule]
})
export class UserSimilarRoutingModule {}
