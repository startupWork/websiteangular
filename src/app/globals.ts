// globals.ts
import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
  url: string = 'https://lookinlabs4halinria.cominlabs.u-bretagneloire.fr';
  ip: string = 'https://omegadatascience.irisa.fr';
  index_pub : string ="publications_inria_swc";
  doc_type_pub: string ="publication";
  index_team: string ="nested_researchteam_inria_sw";
  doc_type_team: string ="nestedResearchteam";
  index_author: string ="nested_inria_author_swci";
  doc_type_author: string ="nestedAuthor";

}
