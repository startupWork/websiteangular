// import { NgModule } from '@angular/core';
// import { Routes, RouterModule }  from '@angular/router';
//
//  const routes: Routes = [
//   { path: '', redirectTo: 'search', pathMatch: 'full' },
//   { path : 'primer', loadChildren : './views/primer.module#PrimerModule'},
//   { path : 'basket', loadChildren : './basket.Module#BasketModule'},
//   { path : 'individual', loadChildren : './profile/profile.module#ProfileModule'},
//   { path : 'team', loadChildren : './affiliations/affiliation-display/affiliationdisplay.module#AffiliationsDisplayModule'},
//   { path: 'individuals-similars', loadChildren: './users/userShared.module#UserSimilarModule'},
//   { path: 'individuals-relations', loadChildren:  './users/userShared.module#UserrelationModule'},
//   { path: 'individuals-team-keyword', loadChildren: './users/userShared.module#UsersAffKeywordsModule' },
//   { path: 'publications-similars', loadChildren: './publications/publications.module#PublicationsSimilarModule' },
//   { path: 'publications-individual-keyword', loadChildren: './publications/publications.module#PublicationModule' },
//   { path: 'publications-team-keyword', loadChildren: './affiliations/affiliations.module#AffiliationKeywordsModule' },
//   { path: 'teams-similar', loadChildren: './affiliations/affiliations.module#AffiliationsModule' }
// ];
//
// @NgModule({
//     imports: [RouterModule.forRoot(routes)],
//     exports: [RouterModule]
// })
// export class AppRoutingModule {}
import { NgModule } from '@angular/core';
import { Routes, RouterModule }  from '@angular/router';

//First page and information
import { AllSearch } from './template-page/All-search/all_search';
import { Profile } from './profile/profile-display/profile';
import { Affiliation } from './affiliations/affiliation-display/affiliation';
import { Primer } from './views/primer';
import { Legal } from './views/legal';
import { Contact } from './views/contact';
import { Credit } from './views/credit';
import { PersonalData } from './views/personaldata';
import { Basket } from './basket/basket-results/basket';

//user
import { UsersSimilar } from './users/users-similars/user-similar';
import { UsersRelations } from './users/users-relations/users-relations';
import { UsersAffKeywords } from './users/users-aff-keywords/users-aff-keywords';

//Publication components
import { PublicationsSimilar } from './publications/publications-similars/publications-similar';
import { PublicationsUserKeyword } from './publications/publications-user-keyword/publications-user-keywords';

//Affiliation Components
import { AffiliationKeywords } from './publications/publication-affiliation-keywords/publication-lab-keywords';
import { AffiliationsSimilar } from './affiliations/affiliation-similar/affiliationsSimilar';

 const routes: Routes = [
  { path: '', redirectTo: 'search', pathMatch: 'full' },
  { path: 'search', component : AllSearch },
  { path : 'primer', component : Primer},
  { path : 'contact', component : Contact},
  { path : 'legal', component : Legal},
  { path : 'credit', component : Credit},
  { path : 'personaldata', component : PersonalData},
  { path : 'basket', component : Basket},
  { path : 'individual/:key_user', component : Profile},
  { path : 'team/:teamid', component : Affiliation},
  { path: 'individuals-similars/:userId', component: UsersSimilar },
  { path: 'individuals-relations/:userId', component: UsersRelations },
  { path: 'publications-similars/:publicationId', component: PublicationsSimilar },
  { path: 'publications-individual-keyword/:userId/:keywords', component: PublicationsUserKeyword },
  { path: 'teams-similar/:id', component: AffiliationsSimilar },
  { path: 'publications-team-keyword/:id/:keywords', component: AffiliationKeywords },
  { path: 'individuals-team-keyword/:id/:keywords', component: UsersAffKeywords }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
