import { NgModule }  from '@angular/core';
import { CommonModule }  from '@angular/common';
import { SharedModule } from '../../shared.module';
import {  PublicationModule} from '../publications.module';
import {PublicationsSimilarRoutingModule } from './publications-similar-routing.module';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";

import { PublicationsSimilar } from './publications-similar';

@NgModule({
  imports: [CommonModule, SharedModule, PublicationModule, PublicationsSimilarRoutingModule, MaterialModule, FormsModule, FlexLayoutModule],
  declarations: [PublicationsSimilar]
})

export class PublicationsSimilarModule {}
