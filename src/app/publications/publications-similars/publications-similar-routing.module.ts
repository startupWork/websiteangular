import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';


import { PublicationsSimilar } from './publications-similar';

@NgModule({
  imports: [RouterModule.forChild([
    { path: ':publicationId', component: PublicationsSimilar},
  ])],
  exports: [RouterModule]
})
export class PublicationsSimilarRoutingModule {}
