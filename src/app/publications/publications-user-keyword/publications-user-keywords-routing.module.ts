import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';


import { PublicationsUserKeyword } from './publications-user-keywords';

@NgModule({
  imports: [RouterModule.forChild([
    { path: ':userId/:keywords', component: PublicationsUserKeyword},
  ])],
  exports: [RouterModule]
})
export class PublicationsUserKeywordRoutingModule {}
