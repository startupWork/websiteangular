import { NgModule }  from '@angular/core';
import { CommonModule }  from '@angular/common';
import { SharedModule } from '../../shared.module';
import {  PublicationModule} from '../publications.module';
import {AffiliationKeywordRoutingModule } from './publication-aff-keywords-routing.module';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";

import { AffiliationKeywords } from './publication-lab-keywords';

@NgModule({
  imports: [CommonModule, SharedModule, PublicationModule, AffiliationKeywordRoutingModule, MaterialModule, FormsModule, FlexLayoutModule],
  declarations: [AffiliationKeywords]
})

export class AffiliationKeywordsModule {}
