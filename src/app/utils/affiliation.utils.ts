export class AffiliationUtils{

    constructor(){}

    /**
     * Get the main 15 keywords of the affiliation
     * @param resp : resp of the request
     * @param : id of the affiliation
     */
    getKeywords(resp, affiliationId){
        let result = resp._source.pubs;
        let mapkeywords = new Map();
        for (let i = 0; i < result.length; i++) {
            // Parcours all the publication and find keywords in en or fr
            for(let j = 0; j < result[i].keywords_en.length; j++) {
                if(result[i].keywords_en[j] != "" && result[i].keywords_en[j] != undefined) {
                    // If we find twice the same word we put +1 for this word
                    if(mapkeywords.has(result[i].keywords_en[j])){
                        let nbwords = mapkeywords.get(result[i].keywords_en[j]);
                        nbwords = nbwords + 1;
                        mapkeywords.set(result[i].keywords_en[j], nbwords);
                    }else{
                        mapkeywords.set(result[i].keywords_en[j], 1);
                    }
                }
            }
            // Same keywords fr and increment the number if appear more tha n one
            for(let k = 0; k < result[i].keywords_fr.length; k++) {
                if(result[i].keywords_fr[k] != "" && result[i].keywords_fr[k] != undefined) {
                    if(mapkeywords.has(result[i].keywords_fr[k])){
                        let nbwords2 = mapkeywords.get(result[i].keywords_fr[k]);
                        nbwords2 = nbwords2 + 1;
                        mapkeywords.set(result[i].keywords_fr[k], nbwords2);
                    }else{
                        mapkeywords.set(result[i].keywords_fr[k], 1);
                    }
                }
            }
        }
        // Change map to array to display the keywords
        let newarray = [];
        mapkeywords.forEach(function(value, key, map)
        {
            newarray.push([key, value]);
        });

        newarray.sort(function(a, b) {
            return b[1] - a[1]
        });
        let words = [];
        //Push the words for the word cloud with the size
        for(let i=0; i < newarray.length; i++) {
            words.push({
                "text": newarray[i][0].toLowerCase(),
                "size": newarray[i][1] + 20,
                "affiliationId": affiliationId
            });
            if (i > 50) {
                break
            }
        }
        return words;
    };

    /**
     * Get the main 15 keywords of the affiliation by domain
     * @param resp : resp of the request
     * @param affiliationId: id of the affiliation
     * @param domain : domain of the publication
     */
    getKeywordsDomain(resp, affiliationId, domain){
        let result = resp._source.pubs;
        let mapkeywords = new Map();
        for (let i = 0; i < result.length; i++) {
            for (let l = 0; l < result[i].hal_domains.length; l++) {
                let splitTitle = result[i].hal_domains[l].title.split("/");
                //Check the part of the hal domain and count them and add them to the map
                if (domain == splitTitle[0]) {
                    // Parcours all the publication and find keywords in en or fr
                    for (let j = 0; j < result[i].keywords_en.length; j++) {
                        if (result[i].keywords_en[j] != "" && result[i].keywords_en[j] != undefined) {
                            // If we find twice the same word we put +1 for this word
                            if (mapkeywords.has(result[i].keywords_en[j])) {
                                let nbwords = mapkeywords.get(result[i].keywords_en[j]);
                                nbwords = nbwords + 1;
                                mapkeywords.set(result[i].keywords_en[j], nbwords);
                            } else {
                                mapkeywords.set(result[i].keywords_en[j], 1);
                            }
                        }
                    }
                    // Same keywords fr and increment the number if appear more tha n one
                    for (let k = 0; k < result[i].keywords_fr.length; k++) {
                        if (result[i].keywords_en[k] != "" && result[i].keywords_en[k] != undefined) {
                            if (mapkeywords.has(result[i].keywords_en[k])) {
                                let nbwords2 = mapkeywords.get(result[i].keywords_en[k]);
                                nbwords2 = nbwords2 + 1;
                                mapkeywords.set(result[i].keywords_en[k], nbwords2);
                            } else {
                                mapkeywords.set(result[i].keywords_en[k], 1);
                            }
                        }
                    }
                }
            }
        }
        // Change map to array to display the keywords
        let newarray = [];
        mapkeywords.forEach(function(value, key, map)
        {
            newarray.push([key, value]);
        });

        newarray.sort(function(a, b) {
            return b[1] - a[1]
        });
        let words = [];
        //Push the words for the word cloud with the size
        for(let i=0; i < newarray.length; i++) {
            words.push({
                "text": newarray[i][0].toLowerCase(),
                "size": newarray[i][1] + 20,
                "affiliationId": affiliationId
            });
            if (i > 50) {
                break
            }
        }
        return words;
    };

    /**
     * Get the main 15 keywords of the affiliation by domain
     * @param resp : resp of the request
     * @param affiliationId : id of the affiliation
     */
    getKeywordsSubDomain(resp, subdomain){
        let result = resp._source.pubs;
        let mapkeywords = new Map();
        for (let i = 0; i < result.length; i++) {
            for (let l = 0; l < result[i].hal_domains.length; l++) {
                //Check the part of the hal domain and count them and add them to the map
                if (subdomain == result[i].hal_domains[l].title.split("/")[1]) {
                    // Parcours all the publication and find keywords in en or fr
                    for (let j = 0; j < result[i].keywords_en.length; j++) {
                        if (result[i].keywords_en[j] != "" && result[i].keywords_en[j] != undefined) {
                            // If we find twice the same word we put +1 for this word
                            if (mapkeywords.has(result[i].keywords_en[j])) {
                                let nbwords = mapkeywords.get(result[i].keywords_en[j]);
                                nbwords = nbwords + 1;
                                mapkeywords.set(result[i].keywords_en[j], nbwords);
                            } else {
                                mapkeywords.set(result[i].keywords_en[j], 1);
                            }
                        }
                    }
                    // Same keywords fr and increment the number if appear more tha n one
                    for (let k = 0; k < result[i].keywords_fr.length; k++) {
                        if (result[i].keywords_fr[k] != "" && result[i].keywords_fr[k] != undefined) {
                            if (mapkeywords.has(result[i].keywords_fr[k])) {
                                let nbwords2 = mapkeywords.get(result[i].keywords_fr[k]);
                                nbwords2 = nbwords2 + 1;
                                mapkeywords.set(result[i].keywords_fr[k], nbwords2);
                            } else {
                                mapkeywords.set(result[i].keywords_fr[k], 1);
                            }
                        }
                    }
                }
            }
        }
        // Change map to array to display the keywords
        let newarray = [];
        mapkeywords.forEach(function(value, key, map)
        {
            newarray.push([key.toLowerCase(), value]);
        });

        newarray.sort(function(a, b) {
            return b[1] - a[1]
        });
        let words = [];
        //Push the words for the word cloud with the size
        for(let i=0; i < newarray.length; i++) {
            words.push(newarray[i][0]);
            if (i > 25) {
                break
            }
        }
        return words;
    };

    /**
     * Get the subDomain of the publication in a affiliation
     * @param resp : response contain publications
     * @param domain : domain to get subDomain
     * @returns {{explains: Array, subDomain: Array, domainSubDomain: Array}} : explains
     */
    getSubDomainAffiliation(resp, domain){
        let result = resp;
        let explains = [];
        let domainsSubDomain = [];
        let mapDomainNb = new Map();
        for (let i = 0; i < result.length; i++) {
            for(let j=0; j < result[i].hal_domains.length; j++){
                // Get the hal domains
                let splitTitle = result[i].hal_domains[j].title.split("/");
                if(splitTitle[0] == domain) {
                    if(splitTitle[2] != undefined){
                        splitTitle[1] = splitTitle[1]+"/"+splitTitle[2];
                    }
                    //Check the part of the hal domain and count them and add them to the map
                    if (mapDomainNb.has(splitTitle[1])) {
                        let nb_pubIn = mapDomainNb.get(splitTitle[1]);
                        mapDomainNb.set(splitTitle[1], nb_pubIn + 1);
                    } else if (splitTitle[1] != "" || splitTitle[1] != undefined) {
                        domainsSubDomain.push(result[i].hal_domains[j].title);
                        mapDomainNb.set(splitTitle[1], 1);
                    }
                }
            }
        }
        // Change the map to array to display with ng-repeat
        let subDomain = [];
        mapDomainNb.forEach(function(value, key, map)
        {
            if(key != undefined){
                subDomain.push([key ,value]);
                explains.push(key);
            }
        });
        subDomain.sort(function(a, b) {
            return b[1] - a[1]});
        return {explains : explains, subDomain : subDomain, domainSubDomain : domainsSubDomain};
    };

    /**
     * Get the domain of the affiliation
     * @param resp : response contain the publication
     * @returns {Array} : Contain the domain of the affiliation class by number
     */
    getDomainAff(resp){
        let result = resp;
        let mapDomain = new Map();
        let mapDomainNb = new Map();
        for (let i = 0; i < result.length; i++) {
            for(let j=0; j < result[i].hal_domains.length; j++){
                // Get the hal domains
                let splitTitle = result[i].hal_domains[j].title.split("/");
                //Check the part of the hal domain and count them and add them to the map
                if(mapDomain.has(splitTitle[0])){
                    let arrayExplain = mapDomain.get(splitTitle[0]);
                    let nb_pubIn = mapDomainNb.get(splitTitle[0]);
                    mapDomainNb.set(splitTitle[0], nb_pubIn + 1);
                    // Check if the second part of the domain is in the array
                    if(!arrayExplain.includes(splitTitle[1]) && splitTitle[1] != undefined) {
                        arrayExplain.push(splitTitle[1]);
                        mapDomain.set(splitTitle[0], arrayExplain);
                    }
                }else if(splitTitle[1] == "" || splitTitle[1] == undefined){
                    mapDomain.set(splitTitle[0], []);
                    mapDomainNb.set(splitTitle[0], 1);
                }else{
                    mapDomain.set(splitTitle[0], [splitTitle[1]]);
                    mapDomainNb.set(splitTitle[0], 1);
                }
            }
        }

        let domains = [];
        // Change the map to array to display with ng-repeat
        mapDomain.forEach((value, key, map) =>
        {
            let nb_pubIn = mapDomainNb.get(key);
            let encodeUri = encodeURIComponent(key);
            domains.push({key : key, value : value, nbPub : nb_pubIn, formatUri : encodeUri});
        });
// toLowerCase
        domains.sort(function(a, b) {
            return b.nbPub - a.nbPub;
        });
        return domains;
    };
}
