import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LocalStorageService{

    public dataChange;
    private observableLocal;

    constructor(){
        this.dataChange = new Observable(
            observer => {this.observableLocal = observer;}
        );
    }

    set(list_data){
        localStorage.setItem("data", JSON.stringify(list_data));
        let temp = localStorage.getItem("data");
        let data = [];
        if (temp != undefined) {
            data = JSON.parse(temp);
        } else {
            data = [];
        }
        this.observableLocal.next(data.length);
    }

}
