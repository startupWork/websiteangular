import { Injectable } from '@angular/core';
import * as elastic from "elasticsearch-browser";
import { ElasticService } from './es-serv';
import {Globals} from '../globals'
/**
* All the functionality to get or search affiliation or publication in Elasticsearch
*/
@Injectable()
export class SearchPubService{

    client : elastic.Client;
    private globals : Globals = new Globals();

    constructor(){
        this.client = new ElasticService().getClient();
    }
    // Get publication
    getPublication(publicationId) {
        return this.client.get<any>({
            index: this.globals.index_pub,
            type: this.globals.doc_type_pub,
            id: publicationId
        });
    }
    //get publication by ids
    getPublicationsById(listId){
      return this.client.search({
        index : this.globals.index_pub,
        type : this.globals.doc_type_pub,
        body : {
          "size" : listId.length,
          "query" : {
            "ids" : {
            "values" : listId
          }
          }
        }
      })
    }
    //Get significant keywords from a list of Id's publication
    getSignWords(list_pub){
        return this.client.search({
            index: this.globals.index_pub,
            type: this.globals.doc_type_pub,
            body: {
                "_source" : false,
                "query" : {
                    "ids" : {
                    "values" : list_pub
                }
            },
                "aggregations" : {
                    "significant_words" : {
                        "significant_terms" : { "field": "title_abstract_sign",
                                            "size": 100 }
                    }
                }
        }
        })
    }
    // Get the publications similar to a given publication
    getSimilarPublications(from, size, publicationId) {
        return this.client.search({
            index: this.globals.index_pub,
            type: this.globals.doc_type_pub,
            body: {
                "from": from,
                "size": size,
                "query": {
                    "bool": {
                        "should": [
                            {
                                "more_like_this": {
                                    "fields": ["abstract_en", "abstract_fr"],
                                    "ids": [publicationId],
                                    "min_term_freq": 1,
                                    "min_word_length": 4
                                }
                            },
                            {
                                "more_like_this": {
                                    "fields": ["title_fr", "title_en"],
                                    "ids": [publicationId],
                                    "min_term_freq": 1,
                                    "min_word_length": 4,
                                    "boost" : 2
                                }
                            },
                            {
                                "more_like_this": {
                                    "fields": ["keywords_fr", "keywords_en"],
                                    "ids": [publicationId],
                                    "min_term_freq": 1,
                                    "min_word_length": 3,
                                    "boost" : 3
                                }
                            },
                            {
                                "more_like_this": {
                                    "fields": ["title_fr.bigrams", "title_en.bigrams"],
                                    "ids": [publicationId],
                                    "min_term_freq": 1,
                                    "min_word_length": 3,
                                    "boost" : 4
                                }
                            },
                            {
                                "more_like_this": {
                                    "fields": ["title_fr.trigrams", "title_en.trigrams"],
                                    "ids": [publicationId],
                                    "min_term_freq": 1,
                                    "min_word_length": 3,
                                    "boost" : 5
                                }
                            }
                        ]
                    }
                }
            }
        });
    }
    // Get the publications related to a given text
    publiTextSearch(from, size, competencies) {
        return this.client.search({
            index: this.globals.index_pub,
            type: this.globals.doc_type_pub,
            body: {
                "from": from,
                "size": size,
                "query": {
                    "bool": {
                        "should": [
                            {
                                "multi_match": {
                                    "fields": [
                                        "abstract_fr",
                                        "abstract_en"
                                    ],
                                    "query": competencies,
                                    "type":  "most_fields"
                                }
                            },
                            {
                                "multi_match": {
                                    "fields": [
                                        "title_fr",
                                        "title_en"
                                    ],
                                    "query": competencies,
                                    "type":  "most_fields",
                                    "boost": 5
                                }
                            },
                            {
                                "match": {
                                    "keywords_en": {
                                        "query": competencies,
                                        "boost": 10
                                    }
                                }
                            },
                            {
                                "match": {
                                    "keywords_fr": {
                                        "query": competencies,
                                        "boost": 10
                                    }
                                }
                            },
                            {
                                "match": {
                                    "title_en.bigrams": {
                                        "query": competencies,
                                        "boost": 20
                                    }
                                }
                            },
                            {
                                "match": {
                                    "title_fr.bigrams": {
                                        "query": competencies,
                                        "boost": 20
                                    }
                                }
                            },
                            {
                                "match": {
                                    "title_en.trigrams": {
                                        "query": competencies,
                                        "boost": 40
                                    }
                                }
                            },
                            {
                                "match": {
                                    "title_fr.trigrams": {
                                        "query": competencies,
                                        "boost": 40
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        });
    }
    getPublicationsWithBigram(from, size, bigrams){
        let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let bigrams_clean_en = [];
        let bigrams_clean_fr = [];
        let newtext = bigrams.match(re_bi);
        for(var i = 0; i < newtext.length; i++){
            bigrams_clean_en[i] = {
                    "match_phrase" : {
                    "keywords_en" : {
                        "query" : newtext[i],
                        "boost" : 2
                    }
                }
            };
            bigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "keywords_fr" : {
                        "query" : newtext[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_pub,
            type: this.globals.doc_type_pub,
            body: {
                "from": from,
                "size": size,
                                "query" : {
                                    "bool": {
                                        "should": [
                                            {
                                                "multi_match" : {
                                                    "query" : bigrams,
                                                    "fields" : ["title_en.bigrams", "title_fr.bigrams"],
                                                    "boost" : 10
                                                }
                                            },
                                            {
                                                "multi_match" : {
                                                    "query" : bigrams,
                                                    "fields" : ["abstract_en.bigrams", "abstract_fr.bigrams"]
                                                }
                                            },
                                            bigrams_clean_en,
                                            bigrams_clean_fr
                                            // {
                                            //     "match_phrase" : {
                                            //         "keywords_en" : {
                                            //             "query" : bigrams,
                                            //             "boost" : 2
                                            //         }
                                            //     }
                                            // },
                                            // {
                                            //     "match_phrase" : {
                                            //         "keywords_fr" : {
                                            //             "query" : bigrams,
                                            //             "boost" : 2
                                            //         }
                                            //     }
                                            // }
                                        ]
                                    }

                }
            }
        }
    )}
    getPublicationsBigramsTrigrams(from, size, trigrams, bigrams) {
        let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let trigrams_clean_en = [];
        let trigrams_clean_fr = [];
        let bigrams_clean_en = [];
        let bigrams_clean_fr = [];
        let newtextbi = bigrams.match(re_bi);
        let newtexttri = trigrams.match(re_tri);
        for(var i = 0; i < newtextbi.length; i++){
            bigrams_clean_en[i] = {
                    "match_phrase" : {
                    "keywords_en" : {
                        "query" : newtextbi[i],
                        "boost" : 2
                    }
                }
            };
            bigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "keywords_fr" : {
                        "query" : newtextbi[i],
                        "boost" : 2
                    }
                }
            };
        }
        for(var i = 0; i < newtexttri.length; i++){
            trigrams_clean_en[i] = {
                    "match_phrase" : {
                    "keywords_en" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
            trigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "keywords_fr" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_author,
            type: this.globals.doc_type_author,
            body: {
                "from": from,
                "size": size,
                        "query":{
                        "bool": {
                            "should": [
                            {"multi_match" : {
                                "query" : trigrams,
                                "fields" : ["title_en.trigrams", "title_fr.trigrams", "abstract_en.trigrams", "abstract_fr.trigrams"]
                            }},
                            {"multi_match" : {
                                "query" : bigrams,
                                "fields" : ["title_en.bigrams", "title_fr.bigrams", "abstract_en.bigrams", "abstract_fr.bigrams"]
                            }},
                            bigrams_clean_en,
                            bigrams_clean_fr,
                            trigrams_clean_en,
                            trigrams_clean_fr
                        ]}}
                    }
        });
    }
    getPublicationsBigramsTrigramsText(from, size, trigrams, bigrams, text) {
        let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let trigrams_clean_en = [];
        let trigrams_clean_fr = [];
        let bigrams_clean_en = [];
        let bigrams_clean_fr = [];
        let newtextbi = bigrams.match(re_bi);
        let newtexttri = trigrams.match(re_tri);
        for(var i = 0; i < newtextbi.length; i++){
            bigrams_clean_en[i] = {
                    "match_phrase" : {
                    "keywords_en" : {
                        "query" : newtextbi[i],
                        "boost" : 2
                    }
                }
            };
            bigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "keywords_fr" : {
                        "query" : newtextbi[i],
                        "boost" : 2
                    }
                }
            };
        }
        for(var i = 0; i < newtexttri.length; i++){
            trigrams_clean_en[i] = {
                    "match_phrase" : {
                    "keywords_en" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
            trigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "keywords_fr" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_author,
            type: this.globals.doc_type_author,
            body: {
                "from": from,
                "size": size,
                "query": {
                        "bool": {
                            "should": [
                            {"multi_match" : {
                                "query" : trigrams,
                                "fields" : ["title_en.trigrams", "title_fr.trigrams", "abstract_en.trigrams", "abstract_fr.trigrams"]
                            }},
                            {"multi_match" : {
                                "query" : bigrams,
                                "fields" : ["title_en.bigrams", "title_fr.bigrams", "abstract_en.bigrams", "abstract_fr.bigrams"]
                            }},
                            bigrams_clean_fr,
                            bigrams_clean_en,
                            trigrams_clean_fr,
                            trigrams_clean_en,
                            {"multi_match" : {
                                "query" : text,
                                "fields" : ["title_en", "title_fr", "abstract_en", "abstract_fr", "keywords_en", "keywords_fr"]
                            }}
                        ]}}

            }
        });
    }
    getPublicationsBigramsText(from, size, bigrams, text) {
        let re_bi = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let bigrams_clean_en = [];
        let bigrams_clean_fr = [];
        let newtext = bigrams.match(re_bi);
        for(var i = 0; i < newtext.length; i++){
            bigrams_clean_en[i] = {
                    "match_phrase" : {
                    "keywords_en" : {
                        "query" : newtext[i],
                        "boost" : 2
                    }
                }
            };
            bigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "keywords_fr" : {
                        "query" : newtext[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_pub,
            type: this.globals.doc_type_pub,
            body: {
                "from": from,
                "size": size,
                        "query":{
                        "bool": {
                            "should": [
                            {"multi_match" : {
                                "query" : bigrams,
                                "fields" : ["title_en.bigrams", "title_fr.bigrams", "abstract_en.bigrams", "abstract_fr.bigrams"]
                            }},
                            bigrams_clean_fr,
                            bigrams_clean_en,
                            {"multi_match" : {
                                "query" : text,
                                "fields" : ["title_en", "title_fr", "abstract_en", "abstract_fr", "keywords_en", "keywords_fr"]
                            }}
                        ]}}
            }
        });
    }
    getPublicationsTrigramsText(from, size, trigrams, text) {
        let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let trigrams_clean_en = [];
        let trigrams_clean_fr = [];
        let newtexttri = trigrams.match(re_tri);
        for(var i = 0; i < newtexttri.length; i++){
            trigrams_clean_en[i] = {
                    "match_phrase" : {
                    "keywords_en" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
            trigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "keywords_fr" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_pub,
            type: this.globals.doc_type_pub,
            body: {
                "from": from,
                "size": size,
                "query": {
                        "bool": {
                            "should": [
                            {"multi_match" : {
                                "query" : trigrams,
                                "fields" : ["title_en.trigrams", "title_fr.trigrams", "abstract_en.trigrams", "abstract_fr.trigrams"]
                            }},
                            {"multi_match" : {
                                "query" : text,
                                "fields" : ["title_en", "title_fr", "abstract_en", "abstract_fr", "keywords_en", "keywords_fr"]
                            }},
                            trigrams_clean_en,
                            trigrams_clean_fr
                        ]}}
            }
        });
    }

    getPublicationsWithTrigram(from, size, trigrams){
        let re_tri = /([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+ ([\wàâçéèêëîïôûùüÿñæœ])+/g
        let trigrams_clean_en = [];
        let trigrams_clean_fr = [];
        let newtexttri = trigrams.match(re_tri);
        for(var i = 0; i < newtexttri.length; i++){
            trigrams_clean_en[i] = {
                    "match_phrase" : {
                    "keywords_en" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
            trigrams_clean_fr[i] = {
                    "match_phrase" : {
                    "keywords_fr" : {
                        "query" : newtexttri[i],
                        "boost" : 2
                    }
                }
            };
        }
        return this.client.search({
            index: this.globals.index_team,
            type: this.globals.doc_type_team,
            body: {
                "from": from,
                "size": size,
                    "query" : {
                                    "bool": {
                                        "should": [
                                            {
                                                "multi_match" : {
                                                    "query" : trigrams,
                                                    "fields" : ["title_en.trigrams", "title_fr.trigrams"],
                                                    "boost" : 10
                                                }
                                            },
                                            {
                                                "multi_match" : {
                                                    "query" : trigrams,
                                                    "fields" : ["abstract_en.trigrams", "abstract_fr.trigrams"]
                                                }
                                            },
                                            trigrams_clean_en,
                                            trigrams_clean_fr
                                        ]
                                    }
                                }
            }
        }
    )}
}
