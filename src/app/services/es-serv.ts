import { Injectable } from '@angular/core';
import * as elastic from 'elasticsearch-browser';
import {Globals} from '../globals'

/* Init Elasticsearch service */

@Injectable()
export class ElasticService{
  private _client : elastic.Client;
  private globals : Globals = new Globals();

  constructor() {
    if(!this._client) this._connect();
  }

  private _connect(){
    this._client = new elastic.Client({
      hosts: [
          // Local es instance
        // "https://omegadatascience.irisa.fr"
        this.globals.ip
      ],
      sniffOnStart: false
    });
  }

  getClient(){
    return this._client;
  }
}
