import { Injectable } from '@angular/core';
import {Globals} from '../globals';
import { HttpClient} from '@angular/common/http';

/**
    Class to send mail
*/

@Injectable()
export class SendMailServive{

    constructor(private http : HttpClient){

    }

    /**
    * Get clusters with k-means
    * @param formatName : format name of the authors
    **/
    setRequest(email , msg){
      return this.http.post( "https://omegadatascience.irisa.fr/send_mail", {
          email : email,
          comment : msg,
          headers: { 'Content-Type': 'text/html; charset=utf-8'}}
      ).toPromise();
    }

}
