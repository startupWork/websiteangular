import { Injectable } from '@angular/core';
import * as elastic from 'elasticsearch-browser';
import { ElasticService } from './es-serv';
import {Globals} from '../globals'
import { HttpClient, HttpHeaders } from '@angular/common/http';
/**
* All the functionality to get or search affiliation or publication in Elasticsearch
*/
@Injectable()
export class ClusteringService{

    private globals : Globals = new Globals();

    constructor(private http : HttpClient){
    }

    /**
    * Get clusters with k-means
    * @param formatName : format name of the authors
    **/
    getkmeans_cluster(formatName){
      return this.http.post( this.globals.ip+"/"+this.globals.index_pub+"/"+this.globals.doc_type_pub+"/_search_with_clusters", {"search_request" : {"_source" : [ "title_en", "abstract_en" ], "query" : {"nested" : {"path" : "authors", "query" : {"match" : {"authors.formatName" : formatName}}}}, "size" : 1000}, "query_hint" : "", "algorithm" : "kmeans","include_hits" : false, "field_mapping" : {"title" : ["_source.title_en"], "content" : ["_source.abstract_en"]}}, {headers: {'Content-Type':'application/json'}}
      ).toPromise()
    }

    /**
    * Get clusters with Lingo algorithms
    * @param formatName : format name of the authors
    **/
    getlingo_cluster(formatName){
        return this.http.post(this.globals.ip+"/"+this.globals.index_pub+"/"+this.globals.doc_type_pub+"/_search_with_clusters", {"search_request" : {"_source" : [ "title_en", "abstract_en" ], "query" : {"nested" : {"path" : "authors", "query" : {"match" : {"authors.formatName" : formatName}}}}, "size" : 1000}, "query_hint" : "", "algorithm" : "lingo", "include_hits" : false,"field_mapping" : {"title" : ["_source.title_en"], "content" : ["_source.abstract_en"]}, "attributes": { "LingoClusteringAlgorithm.desiredClusterCountBase": 20, "TermDocumentMatrixBuilder.titleWordsBoost" : 5}}, {headers: {'Content-Type':'application/json'}}
      ).toPromise();
    }

    /**
    * Get clusters with Lingo algorithms
    * @param acronym : acronym of the team
    **/
    getlingo_cluster_team(acronym){
        return this.http.post(this.globals.ip+"/"+this.globals.index_pub+"/"+this.globals.doc_type_pub+"/_search_with_clusters", {"search_request" : {"_source" : [ "title_en", "abstract_en" ], "query" : {"nested" : {"path" : "affiliations", "query" : {"match" : {"affiliations.acronym" : acronym}}}}, "size" : 1000}, "query_hint" : "", "algorithm" : "lingo", "include_hits" : false,"field_mapping" : {"title" : ["_source.title_en"], "content" : ["_source.abstract_en"]}, "attributes": {"LingoClusteringAlgorithm.desiredClusterCountBase": 20, "TermDocumentMatrixBuilder.titleWordsBoost" : 5}}, {headers: {'Content-Type':'application/json'}}
      ).toPromise();
    }

}
