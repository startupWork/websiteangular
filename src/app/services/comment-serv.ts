import { Injectable } from '@angular/core';
import {Globals} from '../globals';
import { HttpClient, HttpHeaders} from '@angular/common/http';

/**
    Class use to create comment and save user request
*/
@Injectable()
export class CommentService{

    private globals : Globals = new Globals();
    private headers = new HttpHeaders();

    constructor(private http : HttpClient){
    }

    /**
    * Get clusters with k-means
    * @param formatName : format name of the authors
    **/
    setRequest(request){
      let headers = this.headers.set('Content-Type', 'application/json');
      headers = this.headers.set('Accept', '*/*');
      return this.http.post( this.globals.ip+"/request", {"index" : "request_user", "type" : "request",
      "body" : {"request" : request}}, {headers: headers}
      ).toPromise()
    }

      setComment(userId, name, comment){
        return this.http.post( this.globals.ip+"/commentIndiv", {"index" : "comment", "type" : "indiv_comment",
        "body" : {
            "indivId" : userId,
            "name" : name,
            "comment" : comment
        }}
        ).toPromise()
    }

    setCommentAff(affiliationId, name, comment){
          return this.http.post( this.globals.ip+"/commentAff", {"index" : "comment", "type" : "team_comment",
          "body" : {
              "teamId" : affiliationId,
              "name" : name,
              "comment" : comment}}
          ).toPromise()
      }
}
