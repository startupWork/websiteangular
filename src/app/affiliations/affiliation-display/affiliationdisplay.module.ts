import { NgModule }  from '@angular/core';
import { CommonModule }  from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";

import {AffiliationRoutingModule} from './affiliationdisplay-routing.module';

import { SharedModule }       from '../../shared.module';
import {SharedProfileModule} from '../../sharedprofile.module';

import { AffiliationUsers} from '../affiliation-users/affiliation-users';
import { Affiliation } from '../affiliation-display/affiliation';
import { AffiliationDomain } from '../affiliation-domain/affiliation-domain';
import { AffiliationInfo } from '../affiliation-general/affiliation-info';
import { DiagramSubDomain } from '../../d3_template/diagram-subdomain/diagram-subdomain';

//providers
import {SearchLabService} from '../../services/es-lab-serv';

@NgModule({
  imports: [CommonModule, SharedModule, SharedProfileModule, AffiliationRoutingModule, MaterialModule, FlexLayoutModule, FormsModule],
  declarations: [ AffiliationUsers, Affiliation, AffiliationDomain, AffiliationInfo, DiagramSubDomain],
    entryComponents: [],
  providers : [ SearchLabService],
})

export class AffiliationsDisplayModule {}
