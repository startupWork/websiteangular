import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';


import { Affiliation } from './affiliation';

@NgModule({
  imports: [RouterModule.forChild([
    { path: ':teamid', component: Affiliation}])],
  exports: [RouterModule]
})
export class AffiliationRoutingModule {}
