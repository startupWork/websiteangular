import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';
import { async } from '@angular/core/testing';

import {Affiliation} from './affiliation';
// import {SearchLabService} from '../../services/es-lab-serv';

/*
    Test for Affiliation controller Controller
 */

describe('Affiliation Test', () => {
    //
    let searchLabServiceStub = {
        acronym : "ASAP"
    };
    //
    let searchLabServive;
    let fixture;
    let el;
    let de;

    // async beforeEach
    beforeEach(async => {
      TestBed.configureTestingModule({
        declarations: [ Affiliation ], // declare the test component
        // providers:    [ {provide : SearchLabService, useValue: searchLabServiceStub } ]
    }).compileComponents();  // compile template and css

    // UserService from the root injector
    // searchLabServive = TestBed.get(SearchLabService);

      //To know if function has been call
      fixture = TestBed.createComponent(Affiliation);
    //   let searchLabService = fixture.debugElement.injector.get(SearchLabService);
    //   let spy = spyOn(searchLabService, 'getAffiliation')
    //      .and.returnValue(Promise.resolve("ok"));
    de = fixture.debugElement.query(By.css('.name-aff'));
    el = de.nativeElement;

    });

    it('check if result Available', function() {
        fixture.detectChanges();
        const content = el.textContent;
        expect(content).toContain('ASAP');
    });

});
