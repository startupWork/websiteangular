import {Component, Input, OnInit} from '@angular/core';
import {SearchLabService} from '../../services/es-lab-serv';
import {AffiliationUtils} from '../../utils/affiliation.utils';
import {CleanWords} from '../../utils/cleanwords.utils';

/**
 * Use to display publications of affiliation by domain
 */

 @Component({
    selector : 'affiliation-domain',
    templateUrl : './affiliation-domain.html'
 })

export class AffiliationDomain implements OnInit{

    @Input() resp;
    @Input() id;

    pageSize = {count: 5, label: '5 per page '};
    currentPage = 1;
    done = false;
    explains = [];

    allPublications = [];
    filteredPublications = [];
    currentPublications_done = false;

    keywordsSubDom = false;
    wordskey = [];
    wordskeyAll = [];
    wordskeyAll_done = false;
    wordskeyAllDom_done = false;
    domains = [];
    domainsSubDomain;
    domains_done = false;

    searchResp;
    domain;
    aff_number;
    totalItems;
    numPages;
    resultStat;
    currentPublications;
    subDomain;
    subDomainKey;

    cleanWords = new CleanWords();
    affiliationUtils = new AffiliationUtils();

    constructor(private searchLabService : SearchLabService){}

    ngOnInit(){
        if (this.resp != undefined) {
            this.searchResp = this.resp;
            this.domain = this.getDomainAff(this.resp);
            this.getAffPublications(this.resp, this.domain);
            this.getSubDomainAff(this.resp, this.domain);
            this.getKeywords();
        }
    }

    /**
     * Select page to display for publication
     * @param page : number of the page
     */
    selectPage(page){
        this.currentPage = page.numPage;
        this.setCurrentPublications();
    }

    /**
     * Select the new user with new select page size option
     * @param pageSize : nb user to display by page
     */
    selectPageSize(pageSize){
        if(this.currentPage * pageSize.pageSize > this.aff_number){
            this.currentPage = Math.ceil(this.aff_number / pageSize.pageSize)
        }
        this.pageSize = pageSize.pageSize;
        this.setCurrentPublications();
    }

    /**
     * Check if allPublication exist
     * @returns {boolean}
     */
    isAvailableResults() {
        return this.allPublications ? true : false;
    }

    /**
     * Check if at least 1 publication is available
     * @returns {boolean}
     */
    isAtLeastOneResult() {
        if (!this.isAvailableResults()) {
            return false;
        }
        return this.allPublications.length > 0;
    }

    /**
     * Get publication by domain and sort then by date
     * @param resp : response give by elasticsearch request
     */
    getAffPublicationsSubDomain(subDomain) {
        // Sort publications by year
        this.allPublications = [];
        let result = this.searchResp._source.pubs;
        for (let i = 0; i < result.length; i++) {
            for (let j = 0; j < result[i].hal_domains.length; j++) {
                // Get the hal domains
                let splitTitle = result[i].hal_domains[j].title.split("/");
                //Check the part of the hal domain and count them and add them to the map
                if(splitTitle[2] != undefined){
                    splitTitle[1] = splitTitle[1]+"/"+splitTitle[2];
                }
                if (subDomain == splitTitle[1] && this.domain == splitTitle[0] && this.allPublications.indexOf(result[i]) == -1) {
                    this.allPublications.push(result[i]);
                }
            }
        }
        this.allPublications.sort( (a, b) => {
            return b.date - a.date;
        });
        this.filteredPublications = this.allPublications;
        this.totalItems = this.allPublications.length;
        this.selectPage({numPage : 1});
        this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
        this.resultStat = {total : this.allPublications.length, took : 10};
    }

    /**
     * Change domain in the tab nav
     * @param domain : the new domain select by the user
     */
    goto(domain){
        this.wordskeyAll_done = false;
        this.domain = domain.tab.textLabel;
        this.getAffPublications(this.resp, this.domain);
        this.getSubDomainAff(this.resp, this.domain);
        this.getKeywords();
    }

    /**
     * Get publication by domain and sort then by date
     * @param resp : response give by elasticsearch request
     * @param domain : domain select by the user
     */
    private getAffPublications(resp, domain) {
        let domainAff = domain;
        this.done = false;
        this.allPublications = [];
        // Sort publications by year
        let result = resp._source.pubs;
        for (let i = 0; i < result.length; i++) {
            for (let j = 0; j < result[i].hal_domains.length; j++) {
                // Get the hal domains
                let splitTitle = result[i].hal_domains[j].title.split("/");
                //Check the part of the hal domain and count them and add them to the map
                if (domainAff == splitTitle[0] && this.allPublications.indexOf(result[i]) == -1) {
                    this.allPublications.push(result[i]);
                }
            }
        }
        this.allPublications.sort( (a, b) => {
            return b.date - a.date;
        });
        this.filteredPublications = this.allPublications;
        this.totalItems = this.allPublications.length;
        this.selectPage({numPage : 1});
        this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
        this.resultStat = {total : this.allPublications.length, took : 10};
        this.done = true;
    }

    /**
     * Set publications to display
     */
    private setCurrentPublications() {
        let from = (this.currentPage - 1) * this.pageSize.count;
        let to = from + this.pageSize.count;
        this.currentPublications = this.filteredPublications.slice(from, to);
        this.totalItems = this.filteredPublications.length;
        this.numPages = Math.ceil(this.totalItems / this.pageSize.count);
        this.currentPublications_done = true;
    }

    /**
     * Get the domain of work of the affiliation with the hal domains
     * @param resp : affiliations response
     * @param domain : domain select by the user
     */
    private getSubDomainAff(resp, domain){
        this.domains_done = false;
        let result = resp._source.pubs;
        let subDomain = this.affiliationUtils.getSubDomainAffiliation(result, domain);
        this.explains = subDomain.explains;
        this.subDomain = subDomain.subDomain;
        this.domainsSubDomain = subDomain.domainSubDomain;
        this.domains_done = true;
    }

    /**
     * Get the domain of work of the affiliation with the hal domains
     * @param resp : affiliations response
     */
    private getDomainAff(resp){
        let result = resp._source.pubs;
        this.domains = this.affiliationUtils.getDomainAff(result);
        // this.domains_done = true;
        return this.domains[0].key;
    }

    /**
     * Use to show Sub domain keywords when user clik on digrams
     * @param subDomain : sub domain click by user
     */
    showKeywords(subDomain){
        this.subDomainKey = subDomain.subDomain;
        this.searchLabService.getSignificantAllDomainKeywords(this.domain+"/"+subDomain.subDomain, this.id).then(
            resp => {
            this.wordskeyAll = [];
            this.wordskeyAll_done = false;
            let result_en = resp.aggregations.affiliations.affiliationfilter.got_back.most_sig_en.buckets;
            for(let i = 0; i < result_en.length; i++){
                if(result_en[i].score > 2){
                    if(this.wordskeyAll.indexOf(result_en[i].key) == -1){
                        this.wordskeyAll.push(result_en[i].key);
                    }
                }
            }
            this.wordskeyAll_done = true;
        });
        this.keywordsSubDom = true;
    }

    /**
     * Get keywords of domain compare to all publication of the domain
     */
    private getKeywords(){
        this.wordskey = [];
        this.wordskeyAllDom_done = false;
        this.searchLabService.getSignificantKeywordsAffAllSubDomain(this.id, this.domainsSubDomain).then(
            resp => {
            let result = resp.aggregations.affiliations.affiliationfilter.got_back.most_sig_en.buckets;
            for(let i = 0; i < result.length; i++){
                if(result[i].score > 2){
                    if(this.wordskey.indexOf(result[i].key) == -1 && result[i].key != ""){
                        this.wordskey.push(result[i].key);
                    }
                }
            }
            this.wordskeyAllDom_done = true;
        });
    }


        /**
         * Use to check if is a mobile or not to display list instead of cards
         * @returns {boolean} : true if is a mobile
         */
        mobileCheck() {
            let check = false;
            (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor);
            return check;
        };

}
