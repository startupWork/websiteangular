import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';


import { AffiliationsSimilar } from './affiliation-similar/affiliationsSimilar';

@NgModule({
  imports: [RouterModule.forChild([
    { path: ':id', component: AffiliationsSimilar}])],
  exports: [RouterModule]
})
export class AffiliationsSimilarRoutingModule {}
