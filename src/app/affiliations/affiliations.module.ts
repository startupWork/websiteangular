import { NgModule }  from '@angular/core';
import { CommonModule }  from '@angular/common';

import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";

import { SharedModule }       from '../shared.module';
import { AffiliationsSimilarRoutingModule } from './affiliations-routing.module'
//users

import { AffiliationsSimilar } from './affiliation-similar/affiliationsSimilar';

//providers
import {SearchLabService} from '../services/es-lab-serv';

import {SocialGraphLab} from '../d3_template/social-graph/social-graph-lab';

@NgModule({
  imports: [CommonModule, SharedModule, AffiliationsSimilarRoutingModule, MaterialModule, FormsModule, FlexLayoutModule],
  declarations: [AffiliationsSimilar, SocialGraphLab],
  providers : [ SearchLabService],
})

export class AffiliationsModule {}
