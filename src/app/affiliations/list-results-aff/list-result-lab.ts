import {Component, Input, OnChanges, SimpleChange, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import { Location } from '@angular/common';
import {SearchLabService} from '../../services/es-lab-serv';
import {CleanWords} from '../../utils/cleanwords.utils';
import {UserUtils} from '../../utils/user.utils';
import {DialogWordsCloud} from '../../template-page/dialog-words-cloud/dialog-words-cloud';
import {LocalStorageService} from '../../services/localStorageService';
import {Globals} from '../../globals'
import { MatomoTracker } from 'ngx-matomo';

/**
 * Component use to display the list of result find for search affiliation
 * Only a Element
 */

 @Component({
     selector : 'list-results-aff',
     templateUrl : './list-results-aff.html'
 })

export class ListResultsAff implements OnInit{


    @Input()    affiliations;
    @Input()    affSimilar;
    @Input()    isSearch : boolean;
    @Input()    isBigram : boolean;
    @Input()    search : string;

    private globals : Globals = new Globals();
    isSimilar = false;

    mapNbPub = new Map();
    mapHistory = new Map();
    encodesearch;
    wordsallkeycloud;
    searchLabService = new SearchLabService();
    cleanWords = new CleanWords();
    userUtils = new UserUtils();
    waitSimilar;
    isSearchUser;
    users;
    textSearch;

    constructor(private location : Location, private dialog : MatDialog, private localStorageService : LocalStorageService, private matomoTracker : MatomoTracker){}

    ngOnInit(){
        if(this.location.path().split("/")[1] == "teams-similar"){
            this.isSimilar = true;
        }
    }

    /**
    * Go to team profile in new tab
    * @param id : id of the team
    */
    goToAffiliation(id : string){
        window.open("/team/"+id);
    };

    /**
    * Go to individaul Team keywords
    * @param id : id of the team
    * @param e,codesearch : text search by user
    */
    goToPublicationIndivKeywords(id, encodesearch){
        window.open("/individuals-team-keyword/"+id+"/"+encodesearch);
    };

    /**
    * Go to individaul Team keywords
    * @param id : id of the team
    * @param e,codesearch : text search by user
    */
    goToSimilarTeam(id){
      window.open("/teams-similar/"+id);
    };

    /**
    * Go ti new tab with publications relate to team and keywords
    * @param affiliationId : id of the team
    */
    goToPublicationKeywords(affiliationId){
        for(let i = 0; i < this.affiliations.length; i++){
            if(this.affiliations[i]._id == affiliationId){
                sessionStorage.setItem("inner_hits", JSON.stringify(this.affiliations[i].inner_hits));
                break;
            }
        }
        window.open("/publications-team-keyword/"+affiliationId+"/"+this.encodesearch);
        sessionStorage.removeItem("inner_hits");
    };

    /**
     * Use to get the keywords in publications of the affiliation
     * @param msg : message to show on top of dialog
     * @param affiliationId : id of the affiliation
     */
     private open(msg : string, affiliationId) {

        this.wordsallkeycloud = [];

        this.searchLabService.getSignificantKeywordsAffAll(affiliationId).then(function(resp){
            let result_fr = resp.aggregations.affiliations.affiliationfilter.got_back.most_sig_words.buckets;
            let wordsallkeycloud = this.cleanWords.getSignificantKeywords(result_fr, affiliationId);
            this.openDialog(msg, wordsallkeycloud);
        });
    };

    /**
     * Open the dialog with the words get Keywords or Main topics
     * @param msg : message to show on top of dialog
     * @param words : words to display in the word cloud
     * @param bigram : bigram from eplain
     */
    private openDialogCloud(msg : string, words, bigram){
        let dial = this.dialog.open(DialogWordsCloud, {
            data : {msg :"Explanation of the return : " + msg, words : words, bigrams : bigram}
        });
    }

    /**
     * Get explanation for the search affiliation
     * @param affiliationId : id of the affiliation
     */
    showExplanation(affiliationId){
        if(!this.isBigram && !this.affSimilar) {
            this.searchLabService.getAffiliationExplain(affiliationId, this.search).then(
                affiliation => {
                    let explainJson = this.userUtils.affSearchExplain(affiliationId, affiliation);
                    //let words = UserUtils.userSearchExplainFormat(explainJson, affiliationId);
                    let words = this.userUtils.normalizeScoreRequest(explainJson);
                    let msg = this.search;
                    let cleanWords = this.cleanWords.getCleanKeywords(words);
                    this.openDialogCloud(msg, cleanWords.words, cleanWords.bigrams);
                }
            );
        }

        if(this.affSimilar){
            this.waitSimilar = true;
            this.searchLabService.getSimilarExplain(this.affSimilar, affiliationId).then(
                result => {
                    this.waitSimilar = false;
                    let explainJson = this.userUtils.affSearchExplain(affiliationId, result);
                    //let words = this.userUtils.userSearchExplainFormat(explainJson, affiliationId);
                    let cleanWords = this.cleanWords.getCleanLevenshtein(explainJson);
                    let words = this.userUtils.normalizeScore(cleanWords);
                    let msg = "Similar Keywords";
                    this.openDialogCloud(msg, words, []);
                })
        } else if(this.isBigram) {
            let new_text = this.search;
            new_text = new_text.replace('"', '');
            new_text = new_text.replace('"', '');
            let re = /\"(.*?)\"/g;
            let newtext = this.search.match(re);
            let unigrams = "";
            let tri = "";
            let bi = "";
            for(let i = 0; i < newtext.length; i++){
                let biortri = newtext[i];
                unigrams = this.search.replace(biortri, '');
                biortri = biortri.replace('"', '');
                biortri = biortri.replace('"', '');
                let biortri_split = newtext[i].split(" ");
                biortri = biortri.replace('"', '');
                biortri = biortri.replace('"', '');
                if(biortri_split.length == 2){
                    bi = bi + biortri
                }
                if(biortri_split.length == 3){
                    tri = tri + biortri
                }
            }
            if(bi != "" && tri != "" && unigrams.match(/[A-Za-z0-9àâçéèêëîïôûùüÿñæœ]+/g)){
                this.searchLabService.getAffiliationsWithBigramTrigramsTextExplain(affiliationId, bi, tri, unigrams).then(
                     result => {
                        let explainJson = this.userUtils.affserSearchExplainBigrams(affiliationId, result.hits.hits);
                        let words = this.userUtils.normalizeScoreBigrams(explainJson);
                        let cleanWords = this.cleanWords.getCleanLevenshteinBigrams(words);
                        let msg = this.search;
                        this.openDialogCloud(msg, cleanWords, []);
                    })
            }else if(bi != "" && tri != "" && !unigrams.match(/[A-Za-z0-9àâçéèêëîïôûùüÿñæœ]+/g)){
                this.searchLabService.getAffiliationsWithBigramTrigramsExplain(affiliationId, bi, tri).then(
                     result => {
                        let explainJson = this.userUtils.affserSearchExplainBigrams(affiliationId, result.hits.hits);
                        let words = this.userUtils.normalizeScoreBigrams(explainJson);
                        let cleanWords = this.cleanWords.getCleanLevenshteinBigrams(words);
                        let msg = this.search;
                        this.openDialogCloud(msg, cleanWords, []);
                    })
            }else if(bi == "" && tri != "" && unigrams.match(/[A-Za-z0-9àâçéèêëîïôûùüÿñæœ]+/g)){
                this.searchLabService.getAffiliationsWithTrigramTextExplain(affiliationId, tri, unigrams).then(
                     result => {
                        let explainJson = this.userUtils.affserSearchExplainBigrams(affiliationId, result.hits.hits);
                        let words = this.userUtils.normalizeScoreBigrams(explainJson);
                        let cleanWords = this.cleanWords.getCleanLevenshteinBigrams(words);
                        let msg = this.search;
                        this.openDialogCloud(msg, cleanWords, []);
                    })
            }else if(bi != "" && tri == "" && unigrams.match(/[A-Za-z0-9àâçéèêëîïôûùüÿñæœ]+/g)){
                this.searchLabService.getAffiliationsWithBigramTextExplain(affiliationId, bi, unigrams).then(
                     result => {
                        let explainJson = this.userUtils.affserSearchExplainBigrams(affiliationId, result.hits.hits);
                        let words = this.userUtils.normalizeScoreBigrams(explainJson);
                        let cleanWords = this.cleanWords.getCleanLevenshteinBigrams(words);
                        let msg = this.search;
                        this.openDialogCloud(msg, cleanWords, []);
                    })
            }else if(bi != "" && tri == "" && !unigrams.match(/[A-Za-z0-9àâçéèêëîïôûùüÿñæœ]+/g)){
                this.searchLabService.getAffiliationsWithBigramExplain(affiliationId, bi).then(
                     result => {
                        let explainJson = this.userUtils.affserSearchExplainBigrams(affiliationId, result.hits.hits);
                        let words = this.userUtils.normalizeScoreBigrams(explainJson);
                        let cleanWords = this.cleanWords.getCleanLevenshteinBigrams(words);
                        let msg = this.search;
                        this.openDialogCloud(msg, cleanWords, []);
                    })
            }else if(bi == "" && tri != "" && !unigrams.match(/[A-Za-z0-9àâçéèêëîïôûùüÿñæœ]+/g)){
                this.searchLabService.getAffiliationsWithTrigramExplain(affiliationId, tri).then(
                     result => {
                        let explainJson = this.userUtils.affserSearchExplainBigrams(affiliationId, result.hits.hits);
                        let words = this.userUtils.normalizeScoreBigrams(explainJson);
                        let cleanWords = this.cleanWords.getCleanLevenshteinBigrams(words);
                        let msg = this.search;
                        this.openDialogCloud(msg, cleanWords, []);
            })
            }
            // if(new_text.split(" ").length == 2){
            // this.searchLabService.getAffiliationsWithBigramExplain(affiliationId, new_text).then(
            //      result => {
            //         let explainJson = this.userUtils.affserSearchExplainBigrams(affiliationId, result.hits.hits);
            //         let words = this.userUtils.normalizeScoreBigrams(explainJson);
            //         let cleanWords = this.cleanWords.getCleanLevenshteinBigrams(words);
            //         let msg = this.search;
            //         this.openDialogCloud(msg, cleanWords, []);
            //     })
            // }else{
            //     this.searchLabService.getAffiliationsWithTrigramExplain(affiliationId, new_text).then(
            //          result => {
            //             let explainJson = this.userUtils.affserSearchExplainBigrams(affiliationId, result.hits.hits);
            //             let words = this.userUtils.normalizeScoreBigrams(explainJson);
            //             let cleanWords = this.cleanWords.getCleanLevenshteinBigrams(words);
            //             let msg = this.search;
            //             this.openDialogCloud(msg, cleanWords, []);
            // })
            //         }
                }
            }


    /**
     * saveAffiliation : Save a affiliation in the basket
     * @param affiliationId : Id of the affiliation
     * @param name : name of the affiliation
     */
    saveAffiliation(affiliationId, name) {
        let data = localStorage.getItem("data");
        let list_data = [];
        if (data == undefined || data == "undefined") {
            list_data = [];
        } else {
            list_data = JSON.parse(data);
        }
        for (let i in list_data) {
            if (list_data[i].id == affiliationId) {
                return;
            }
        }
        if(this.isSearch){
            list_data.push({
                "id": affiliationId,
                "name": name,
                "link": this.globals.url+"/team/" + affiliationId,
                "type": "team",
                "search" : this.search
            });
        }else{
            list_data.push({
                "id": affiliationId,
                "name": name,
                "link": this.globals.url+"/team/" + affiliationId,
                "type": "team",
                "search" : ""
            });
        }
        this.matomoTracker.trackEvent('Team_save', affiliationId, this.search);
        this.localStorageService.set(list_data);
    };

    piwikEvent(teamId){
        this.matomoTracker.trackEvent('Team_click', teamId, this.search);
    }

    /**
     * Check if mobile
     * @returns {boolean} : return true if mobile
     */
    mobileCheck() {
        let check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor);
        return check;
    };

        /**
        * Encode the search of the user when it change
        * @param optionChange : get the new search if the user
        */
        ngOnChanges(changes: {[propKey: string] : SimpleChange}) {
            for (let propName in changes) {
            if (propName == 'affiliations') {
                    this.affiliations = changes['affiliations'].currentValue;
                    for(let i=0; i < this.affiliations.length; i++){
                      if(this.affiliations[i]._source.history_bastri != undefined){
                        if(this.affiliations[i]._source.status == "OLD" && this.affiliations[i]._source.history_bastri.length != 0){
                                let first_pas = false;
                                let history_bastri = this.affiliations[i]._source.history_bastri;
                                let new_name = "";
                                for(let i = 0; i < history_bastri.length; i++){
                                    if(i == 0 && history_bastri.length > 1){
                                        new_name = new_name + " " +history_bastri[i].name + " -> ";
                                    }else if(history_bastri[i].split == "N" && history_bastri.length-1 == i){
                                        if(history_bastri[i].status == "VALID"){
                                            new_name = new_name + " blod" + history_bastri[i].name;
                                        }else{
                                            new_name = new_name + " " + history_bastri[i].name;
                                        }
                                    }else if(history_bastri[i].split == "N"){
                                        if(history_bastri[i].status == "VALID"){
                                            new_name = new_name + " blod" + history_bastri[i].name+ " -> ";
                                        }else{
                                            new_name = new_name + " " + history_bastri[i].name+ " -> ";
                                        }
                                    }
                                    else if(history_bastri[i].split == "Y" && !first_pas){
                                        first_pas = true;
                                        if(history_bastri[i].status == "VALID") {
                                            new_name = new_name + "( blod" + history_bastri[i].name + ", ";
                                        }else if(history_bastri.length-1 == i){
                                              new_name = new_name + history_bastri[i].name;
                                          }
                                          else{
                                            new_name = new_name + "( " + history_bastri[i].name + ", ";
                                        }
                                    }else if(history_bastri[i].split == "Y" && history_bastri.length - 1 == i){
                                        if(history_bastri[i].status == "VALID"){
                                            new_name = new_name +"blod"+ history_bastri[i].name+ " )";
                                        }else{
                                            new_name = new_name + history_bastri[i].name+ " )";
                                        }
                                    }else if(history_bastri[i].split == "Y"){
                                        if(history_bastri[i].status == "VALID"){
                                        new_name = new_name +"blod"+ history_bastri[i].name+ ", ";
                                      }else if(history_bastri.length-1 == i){
                                            new_name = new_name + history_bastri[i].name;
                                        }else{
                                          new_name = new_name + history_bastri[i].name+ ", ";
                                        }
                                    }else{
                                        new_name = history_bastri[i].name;
                                    }
                                }
                                let idnexOf = new_name.indexOf(this.affiliations[i]._source.acronym);
                                new_name = new_name.slice(idnexOf);
                                if(new_name.lastIndexOf(" -> ") == new_name.length - 4){
                                    this.mapHistory.set(this.affiliations[i]._id, this.affiliations[i]._source.acronym);
                                }else if(new_name.lastIndexOf(" -> ") != new_name.indexOf(" -> ")){
                                    new_name = new_name.slice(new_name.indexOf(" -> ")+4, new_name.lastIndexOf(" -> "));
                                    this.mapHistory.set(this.affiliations[i]._id, new_name);
                                }else{
                                    this.mapHistory.set(this.affiliations[i]._id, new_name);
                                }

                    }}
                }
                    if(this.isSearch && !this.isBigram){
                        this.affiliations = changes['affiliations'].currentValue;
                        for(let i=0; i < this.affiliations.length; i++){
                            this.searchLabService.getPublicationByAffiliationKeywords(0, 1, this.affiliations[i]._id, this.search).then(
                                result_pub => {
                                    if(result_pub.hits.hits[0]){
                                        this.mapNbPub.set(result_pub.hits.hits[0].inner_hits.affiliations.hits.hits[0]._source.id,result_pub.hits.total);
                                    }
                                },
                                err =>{

                                }
                            );
                        }
                    }
                    if(this.isSearch && this.isBigram){
                        let new_text = this.search;
                        new_text = new_text.replace('"', '');
                        new_text = new_text.replace('"', '');
                        let re = /\"(.*?)\"/g;
                        let newtext = this.search.match(re);
                        let unigrams = "";
                        let tri = "";
                        let bi = "";
                        for(let i = 0; i < newtext.length; i++){
                            let biortri = newtext[i];
                            unigrams = this.search.replace(biortri, '');
                            biortri = biortri.replace('"', '');
                            biortri = biortri.replace('"', '');
                            let biortri_split = newtext[i].split(" ");
                            biortri = biortri.replace('"', '');
                            biortri = biortri.replace('"', '');
                            if(biortri_split.length == 2){
                                bi = bi + biortri
                            }
                            if(biortri_split.length == 3){
                                tri = tri + biortri
                            }
                        }
                        this.affiliations = changes['affiliations'].currentValue;
                        if(bi != "" && tri != "" && unigrams.match(/[A-Za-z0-9]+/g)){
                            for(let i=0; i < this.affiliations.length; i++){
                                this.searchLabService.getPublicationBySignificantBigramsTrigramsText(0, 1, this.affiliations[i]._id, tri, bi, unigrams).then(
                                    result_pub => {
                                        if(result_pub.hits.hits[0]){
                                            this.mapNbPub.set(result_pub.hits.hits[0]._id, result_pub.hits.hits[0].inner_hits.pubs.hits.total);
                                        }
                                    },
                                    err =>{

                                    }
                                );
                            }
                        }else if(bi != "" && tri != "" && !unigrams.match(/[A-Za-z0-9]+/g)){
                            for(let i=0; i < this.affiliations.length; i++){
                                this.searchLabService.getPublicationBySignificantBigramsTrigrams(0, 1, this.affiliations[i]._id, tri, bi).then(
                                    result_pub => {
                                        if(result_pub.hits.hits[0]){
                                            this.mapNbPub.set(result_pub.hits.hits[0]._id, result_pub.hits.hits[0].inner_hits.pubs.hits.total);
                                        }
                                    },
                                    err =>{

                                    }
                                );
                            }
                        }else if(bi == "" && tri != "" && unigrams.match(/[A-Za-z0-9]+/g)){
                            for(let i=0; i < this.affiliations.length; i++){
                                this.searchLabService.getPublicationBySignificantTrigramsText(0, 1, this.affiliations[i]._id, tri, unigrams).then(
                                    result_pub => {
                                        if(result_pub.hits.hits[0]){
                                            this.mapNbPub.set(result_pub.hits.hits[0]._id, result_pub.hits.hits[0].inner_hits.pubs.hits.total);
                                        }
                                    },
                                    err =>{

                                    }
                                );
                            }
                        }else if(bi != "" && tri == "" && unigrams.match(/[A-Za-z0-9]+/g)){
                            for(let i=0; i < this.affiliations.length; i++){
                                this.searchLabService.getPublicationBySignificantTBigramsText(0, 1, this.affiliations[i]._id, bi, unigrams).then(
                                    result_pub => {
                                        if(result_pub.hits.hits[0]){
                                            this.mapNbPub.set(result_pub.hits.hits[0]._id, result_pub.hits.hits[0].inner_hits.pubs.hits.total);
                                        }
                                    },
                                    err =>{

                                    }
                                );
                            }
                        }else if(bi != "" && tri == "" && !unigrams.match(/[A-Za-z0-9]+/g)){
                            for(let i=0; i < this.affiliations.length; i++){
                                this.searchLabService.getPublicationBySignificantBigrams(0, 1, this.affiliations[i]._id, bi).then(
                                    result_pub => {
                                        if(result_pub.hits.hits[0]){
                                            this.mapNbPub.set(result_pub.hits.hits[0]._id, result_pub.hits.hits[0].inner_hits.pubs.hits.total);
                                        }
                                    },
                                    err =>{

                                    }
                                );
                            }
                        }else if(bi == "" && tri != "" && !unigrams.match(/[A-Za-z0-9]+/g)){
                            for(let i=0; i < this.affiliations.length; i++){
                                this.searchLabService.getPublicationBySignificantTrigrams(0, 1, this.affiliations[i]._id, tri).then(
                                    result_pub => {
                                        if(result_pub.hits.hits[0]){
                                            this.mapNbPub.set(result_pub.hits.hits[0]._id, result_pub.hits.hits[0].inner_hits.pubs.hits.total);
                                        }
                                    },
                                    err =>{

                                    }
                                );
                            }
                        }
                    //     let text = this.search;
                    //     text = text.replace('"', '');
                    //     text = text.replace('"', '');
                    //     this.affiliations = changes['affiliations'].currentValue;
                    //     if(text.split(" ").length == 2){
                    //     for(let i=0; i < this.affiliations.length; i++){
                    //         this.searchLabService.getPublicationBySignificantBigrams(0, 1, this.affiliations[i]._id, text).then(
                    //             result_pub => {
                    //                 if(result_pub.hits.hits[0]){
                    //                     this.mapNbPub.set(result_pub.hits.hits[0]._id, result_pub.hits.hits[0].inner_hits.pubs.hits.total);
                    //                 }
                    //             },
                    //             err =>{
                    //
                    //             }
                    //         );
                    //     }
                    // }else{
                    //     for(let i=0; i < this.affiliations.length; i++){
                    //         this.searchLabService.getPublicationBySignificantTrigrams(0, 1, this.affiliations[i]._id, text).then(
                    //             result_pub => {
                    //                 if(result_pub.hits.hits[0]){
                    //                     this.mapNbPub.set(result_pub.hits.hits[0]._id, result_pub.hits.hits[0].inner_hits.pubs.hits.total);
                    //                 }
                    //             },
                    //             err =>{
                    //
                    //             }
                    //         );
                    //     }
                    // }
                }
                }else if(propName == 'search'){
                        this.encodesearch = encodeURIComponent(changes['search'].currentValue);
                    }
                }
            }
        }
