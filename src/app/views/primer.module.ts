import { NgModule }  from '@angular/core';
import { CommonModule }  from '@angular/common';

import { SharedModule }       from '../shared.module';
import { PrimerRoutingModule } from './primer-routing.module';

//providers
import {Primer} from './primer';


@NgModule({
  imports: [CommonModule, PrimerRoutingModule, SharedModule],
  declarations: [ Primer]
})

export class PrimerModule {}
