import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';


import { Primer } from './primer';

@NgModule({
  imports: [RouterModule.forChild([
    { path: '', component: Primer, pathMatch :'full'}
  ])],
  exports: [RouterModule]
})
export class PrimerRoutingModule {}
