import {Component, OnInit} from '@angular/core';

@Component({
    selector : 'cookies',
    templateUrl : './cookies.html'
})

export class Cookies implements OnInit{

    test = true;

    constructor(){

    }

    clickOnOf(){
        this.test = false;
        localStorage.setItem("cookies", JSON.stringify('{"cookies" : "ok"}'));
    }

    clickOff(){
        this.test = false;
        localStorage.setItem("cookies", JSON.stringify('{"cookies" : "ko"}'));
    }

    ngOnInit(){
        let data = localStorage.getItem("cookies");
        let list_data = [];
        if (data == undefined || data == "undefined") {
            this.test = true;
        } else {
            this.test = false;
        }
    }
}
