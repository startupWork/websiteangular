// import { NgModule }  from '@angular/core';
// import { CommonModule }  from '@angular/common';
// import { MaterialModule } from '@angular/material';
// import { FormsModule } from '@angular/forms';
// import { FlexLayoutModule } from "@angular/flex-layout";
// // import { BrowserModule} from '@angular/platform-browser';
// // import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// // import { Angular2FontawesomeModule } from 'angular2-fontawesome';
// import { AllSearchRoutingModule } from './all_search-routing.module';
//
// import { SharedModule }       from '../../shared.module';
//
// //search page
// import { AllSearch } from './all_search';
//
// //teams
// import {AffiliationsSearch} from '../../affiliations/affiliations-search/affiliations-search';
//
// //users
// import { UsersSearch } from '../../users/users-search/users-search';
//
// //publication
// import {PublicationsSearch} from '../../publications/publications-search/publications-search';
//
// //providers
// import {SearchIndivService} from '../../services/es-indiv-serv';
// import {SearchPubService} from '../../services/es-pub-serv';
// import {SearchLabService} from '../../services/es-lab-serv';
//
// @NgModule({
//   imports: [CommonModule, AllSearchRoutingModule, SharedModule, MaterialModule, FormsModule, FlexLayoutModule],
//   declarations: [ AllSearch, UsersSearch, AffiliationsSearch, PublicationsSearch],
//   providers : [ SearchIndivService, SearchPubService, SearchLabService],
// })
//
// export class AllSearchModule {}
