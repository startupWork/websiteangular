import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';

import { AllSearch }    from './all_search';

@NgModule({
  imports: [RouterModule.forChild([
    { path: 'search', component: AllSearch }
  ])],
  exports: [RouterModule]
})
export class AllSearchRoutingModule {}
