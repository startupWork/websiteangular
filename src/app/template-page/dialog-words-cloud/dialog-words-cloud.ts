import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'words-cloud',
  template: `
  <word-cloud-dir [msg]="msg" [wordsModel]="words" [bigrams]="bigrams">
  </word-cloud-dir>
  `
})
export class DialogWordsCloud {
    msg;
    words;
    bigrams;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
      this.msg = data.msg;
      this.words = data.words;
      this.bigrams = data.bigrams;
  }
}
