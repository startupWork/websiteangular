import {Component, Input, EventEmitter, Output, OnChanges, SimpleChange} from '@angular/core';

/**
 * Use to display pagination and let the user choose the page
 */
 @Component({
     selector : 'pagination',
     templateUrl : './pagination.html'
 })

export class Pagination {

    @Input() currentPage : number;
    @Input() numPages : number;
    @Output() onSelect = new EventEmitter();

    tabs = [];
    morethan : boolean= false;
    currentPageSub : number;

    constructor(){
    }

    /**
    * Select page emit change page
    * @param numPage : num of page select by user
    **/
    selectPage(numPage){
        this.onSelect.emit({numPage : numPage.index + 1});
        if(numPage.index + 1 > (this.tabs.length - 30) && this.morethan && numPage.index + 1 < this.numPages){
            this.tabs.push(this.tabs.length + 1);
        }
    }

    ngOnChanges(changes: {[propKey: string]: SimpleChange}){
        for (let propName in changes) {
        if(propName == 'currentPage'){
                this.currentPageSub = changes['currentPage'].currentValue - 1;
            }
        if(propName == 'numPages'){
                if(changes['numPages'].currentValue < 30){
                    this.numPages = changes['numPages'].currentValue;
                    for(let i=0; i < this.numPages; i++){
                        this.tabs.push(i+1);
                    }
                }else if(changes['numPages'].currentValue > 30){
                    for(let i=0; i < 30; i++){
                        this.tabs.push(i+1);
                    }
                    this.morethan = true;
                }
        }
    }
    }

}
