import {Component, Input} from '@angular/core';
import {SearchPubService} from '../../services/es-pub-serv';

 @Component({
     selector : 'show-words-click',
     templateUrl : './show-words-click.html',
     styleUrls : ['./show-words.css']
 })

export class ShowWordsClick{

    //Word to display
    @Input() words;
    @Input() words_pub;
    listPublications;
    listPublications_done = false;
    morewords = false;
    words_cluster = "";
    listWords_done = false;
    listWords = [];
    nbwords = 15;;

    constructor(private searchService : SearchPubService){}

   /**
    * Use to display word (less or more)
    * @param val : boolean set to true if you need more word else set to false
    */
   moreWords(val) {
       this.morewords = val;
       if(val){
           this.nbwords = this.words.length;
       }else{
           this.nbwords = 15;
       }
   }

   /**
   * Use to display publications and keywords from a cluster Lingo
   * @param word : word click by user to show pub and keywords from cluster
   **/
   gotopub(word){
     this.listPublications_done = false;
     this.listWords_done = false;
     this.words_cluster = word;
     let list_pub = this.words_pub[word];

     this.searchService.getPublicationsById(list_pub).then(
       resp => {
        this.listPublications = resp.hits.hits;
        this.listPublications.sort( (a, b) => {
            return b._source.year - a._source.year;
        });
        this.listWords = this.getKeywords(this.listPublications);
        this.listPublications_done = true;
        this.searchService.getSignWords(list_pub).then(
            resp => {
                let i = 0;
                for(let sign of resp.aggregations.significant_words.buckets){
                    this.listWords.push(sign["key"]);
                    if(i > 6){
                        break;
                    }
                    i++;
                }
                this.listWords_done = true;
            }
        )
       }
     )
   }

   hide(){
     this.listPublications_done = false;
   }

   /**
    * Get the main 15 keywords of the affiliation
    * @param resp : resp of the request
    * @param : id of the affiliation
    */
   private getKeywords(resp){
       let result = resp;
       let mapkeywords = new Map();
       for (let i = 0; i < result.length; i++) {
           // Parcours all the publication and find keywords in en or fr
           for(let j = 0; j < result[i]._source.keywords_en.length; j++) {
               if(result[i]._source.keywords_en[j] != undefined && result[i]._source.keywords_en[j] != "") {
                   // If we find twice the same word we put +1 for this word
                   if(mapkeywords.has(result[i]._source.keywords_en[j])){
                       let nbwords = mapkeywords.get(result[i]._source.keywords_en[j].toLowerCase());
                       nbwords = nbwords + 1;
                       mapkeywords.set(result[i]._source.keywords_en[j].toLowerCase(), nbwords);
                   }else{
                       mapkeywords.set(result[i]._source.keywords_en[j].toLowerCase(), 1);
                   }
               }
           }
       }
       // Change map to array to display the keywords
       let newarray = [];
       mapkeywords.forEach(function(value, key, map)
       {
           newarray.push([key, value]);
       });

       newarray.sort(function(a, b) {
           return b[1] - a[1]
       });
       let words = [];
       //Push the words for the word cloud with the size
       for(let i=0; i < newarray.length; i++) {
           words.push(newarray[i][0]);
           if(i > 5){
               break;
           }
       }
       return words;
   }
}
