import {Component, Input, OnChanges, SimpleChanges, Inject} from '@angular/core';
import {MatDialog} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';
import {LocalStorageService} from '../../services/localStorageService';
import { MatomoTracker } from 'ngx-matomo';

/**
 *  Component to display publication affiliation
 *  Use to share data between affiliation controller and this component
 */

 @Component({
     selector : 'publications',
     templateUrl : './publication-display.html'
 })

export class PublicationAffDisplay{

    //Publication to display
    @Input() publications;
    @Input() search;
    private window: Window;

    constructor(private dialog: MatDialog, private localSorageService : LocalStorageService, private matomoTracker: MatomoTracker){}

    /**
     * Save the publication in the basket
     * @param pubId : Id of the publications
     * @param pub : to get title fr or en  of the publication
     */
    savePublicationAff(pub) {
        let data = localStorage.getItem("data");
        let list_save = [];
        if (data == undefined || data == "undefined") {
            list_save = [];
        } else {
            list_save = JSON.parse(data);
        }
        for (var i in list_save) {
            if (list_save[i].id == pub.halId) {
                return;
            }
        }
        if(this.search){
        if(pub.title_en == ""){
            if(pub.pdflink != "") {
                list_save.push({
                    "id": pub.halId,
                    "name": pub.title_fr,
                    "link": pub.pdflink,
                    "type": "pub",
                    "search" : this.search
                });
            }else{
                list_save.push({
                    "id": pub.halId,
                    "name": pub.title_fr,
                    "link": pub.hallink,
                    "type": "pub",
                    "search" : this.search
                });
            }
        }else {
            if(pub.pdflink != "") {
                list_save.push({
                    "id": pub.halId,
                    "name": pub.title_en,
                    "link": pub.pdflink,
                    "type": "pub",
                    "search" : this.search
                });
            }else{
                list_save.push({
                    "id": pub.halId,
                    "name": pub.title_en,
                    "link": pub.hallink,
                    "type": "pub",
                    "search" : this.search
                });
            }
        }
    }else{
                if(pub.title_en == ""){
                    if(pub.pdflink != "") {
                        list_save.push({
                            "id": pub.halId,
                            "name": pub.title_fr,
                            "link": pub.pdflink,
                            "type": "pub",
                            "search" : ""
                        });
                    }else{
                        list_save.push({
                            "id": pub.halId,
                            "name": pub.title_fr,
                            "link": pub.hallink,
                            "type": "pub",
                            "search" : ""
                        });
                    }
                }else {
                    if(pub.pdflink != "") {
                        list_save.push({
                            "id": pub.halId,
                            "name": pub.title_en,
                            "link": pub.pdflink,
                            "type": "pub",
                            "search" : ""
                        });
                    }else{
                        list_save.push({
                            "id": pub.halId,
                            "name": pub.title_en,
                            "link": pub.hallink,
                            "type": "pub",
                            "search" : ""
                        });
                    }
                }
            }
        this.matomoTracker.trackEvent('Pub_save', pub.halId, this.search);
        this.localSorageService.set(list_save);
    };

    /**
     * Call when user click on external link Make the search in a new tab on hal website
     * @param doi : digital object identifier
     */
    searchInHal(hallink){
        if(hallink != ""){
            window.open(hallink, '_blank');
        }
    };

    goToSimilarPub(pub){
            window.open("/publications-similars/"+pub.halId, '_blank');
        }

    /**
     * Show abstract of the publication if available
     * @param pub : to get abstract if exist
     */
    showAbstract(pub){
        if(pub.abstract_en != ""){
            let dial = this.dialog.open(DialogAbstract, {
                data : {msg : pub.abstract_en}});
        }
        else if(pub.abstract_fr != "") {
            let dial = this.dialog.open(DialogAbstract, {
                data : {msg : pub.abstract_fr}});
        }
        this.matomoTracker.trackEvent('click_pub', this.search, pub.halId);
    }

    /**
     * Change publication when user change page
     * @param optionChange : get the new current publication
     */
    ngOnChanges(optionChange: SimpleChanges) {
        if (optionChange.hasOwnProperty('publications')) {
            if (optionChange.publications.currentValue != undefined) {
                this.publications = optionChange.publications.currentValue;
                if(this.publications.length != 0 && this.publications[0].hasOwnProperty("_source")){
                    var publicationFilter = [];
                    for(var i=0; i< this.publications.length; i++){
                        publicationFilter.push(this.publications[i]._source)
                    }
                    this.publications = publicationFilter;
                }else{
                    this.publications = optionChange.publications.currentValue;
                }
            }
        }
    };
}

/**
* Class user to displau abstract in dialog

<mat-dialog-actions fxLayoutAlign="end">
  <button mat-button mat-dialog-close fxLayoutAlign="end">Close</button>
</mat-dialog-actions>
*/
@Component({
  selector: 'abstract_dial',
  template: `
  <div fxLayout="row">
      <h2 mat-dialog-title fxFlex="98">Abstract</h2>
      <mat-dialog-actions fxLayoutAlign="center" style="margin-bottom:0px; padding : 0px;">
        <button mat-button mat-dialog-close style="height : 30px; width : 30px; background-color : white; border : 0px;">
        <mat-icon class="fill-icon" size="24" style="color : black;">close</mat-icon></button>
      </mat-dialog-actions>
  </div>
  <mat-dialog-content>
      {{ msg }}
  </mat-dialog-content>

  `
})
export class DialogAbstract {
  msg;
  constructor(@Inject(MAT_DIALOG_DATA) msg) {
      if(msg.msg != undefined){
          this.msg = msg.msg;
      }else{
          this.msg = "No abstract for this publication";
      }
  }
}
