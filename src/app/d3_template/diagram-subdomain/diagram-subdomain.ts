import {Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChange} from '@angular/core';
import * as d3 from 'd3';

/**
* Use to diplay user relation in graph use d3 fxLayout
* Only Element
*/


@Component({
    selector : 'diagram-sub-domain',
    template: "<div fxLayoutAlign='center center' id='chart'></div>"
})

export class DiagramSubDomain implements OnInit{

    @Input() subDomain;
    @Output() onClick  = new EventEmitter();

    constructor(){}

    ngOnInit(){
      this.create_diagram();
    }

    create_diagram(){
        if(d3.select("#chart").select("svg") != undefined){
            d3.select("#chart").select("svg").remove();
        }
        let subdo = this.subDomain;
        let data = [];
        for(let i=0; i < subdo.length; i++){
            data.push(subdo[i][1]);
            if(i > 10){
                break;
            }
        }

        let color = d3.schemeCategory10;

        let width = 500,
        height = 300;

        let outerRadius = height / 2 - 20,
        innerRadius = outerRadius / 3,
        cornerRadius = 10;

        let pie = d3.pie()
        .padAngle(.02);

        let arc = d3.arc()
        .padRadius(outerRadius)
        .innerRadius(innerRadius);


        let svg = d3.select("#chart").append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(250, 150)");

        svg.selectAll("path")
        .data(pie(data))
        .enter().append("path")
        .each(function (d) {
            let test = d as any;
            test.outerRadius = outerRadius - 20;
        })
        .attr("fill", (d, i) => {
            return color[i];
        })
        .attr("d", <any>arc)
        .attr("id", (d,i) => { return "domainArc_"+i; })
        .on("mouseover",
        arcTween(outerRadius, 0, false, subdo)
    )
    .on("mouseout",
    arcTween(outerRadius - 20, 10, true, subdo))
    .on("click",
    (d,i) => {
        this.onClick.emit({subDomain : subdo[i][0]});
    });

    //Append the number of publications to each slice
    let k = 0;
    svg.selectAll(".path")
    .data(data)
    .enter().append("text")
    .attr("style", "font-size : 16px;")
    .append("textPath")
    .attr("xlink:href",function(d,i){return "#domainArc_"+i;})
    // .text(function(d){
    //     if(k < 2){
    //         k += 1;
    //         return d;}});

    function arcTween(outerRadius, delay, remove, subDomain) {
        return function (m, i) {
            d3.select(("[id='" + "domainArc_"+i + "']")).transition().delay(delay).attrTween("d",  d => {
            let k = i;
            if(remove){
                svg.select("#textdom").remove();
            }else{
                svg.append("text")
                .attr("id", "textdom")
                .attr("dy", ".5em")
                .style("text-anchor", "middle")
                .style("font-size", "20px")
                .attr("class","label")
                .style("fill", function(){return "black";})
                .text(function(){
                    return subDomain[k][0]});
                }
                let test = d as any;
                let j = d3.interpolate(test.outerRadius, outerRadius);
                return function (t) {
                    test.outerRadius = j(t);
                    return arc(test);
                };
            });
        };
    }
    }

    ngOnChanges(changes: {[propKey: string] : SimpleChange}) {
        for (let propName in changes) {
        if (propName == 'subDomain') {
          this.subDomain = changes['subDomain'].currentValue;
          this.create_diagram();
        }
      }
    }
}
