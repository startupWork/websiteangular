import {Component, Input, OnInit} from '@angular/core';
import * as d34 from 'd3';
import * as d3cloud from 'd3-cloud';

let d3: any = d34;
// Directive to display the graph of number of publications by years

@Component({
  selector : 'word-cloud-aff',
  templateUrl: './word-cloud-aff.html'
})

export class WordCloudAff implements OnInit{

  @Input() wordsModel;
  @Input() msg;

  done = false;

  constructor(){}

  ngOnInit(){
    this.createWordsCloud(this.wordsModel);
    this.done = true;
  }

  createWordsCloud(wordsModel)  {
    let fill = d3.schemeCategory10;

    let selected, color, lastSelected, lastColor;

    let width = window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth;

    let height = window.innerHeight
    || document.documentElement.clientHeight
    || document.body.clientHeight;

    if (width >= 700) {
      width = 700;
    }
    if (height >= 350) {
      height = 350;
    }
    if (width < 700) {
      width = width - 75;
    }
    if (height < 350) {
      height = height - 50;
    }

    if(this.wordsModel.length == 1){
        this.wordsModel[0].size = 18;
    }
    
    let maxsize = 30;
    let minsize = 18;
    d3cloud.size([width, height])
        .words(this.wordsModel)
        .padding(5)
        .fontSize(function (d) {
            /*if(minsize > d.size || d.size > maxsize){
            return (maxsize + Math.floor(Math.random() * 10) + Math.random());
        }else{*/
            return (d.size);
            }
        )
        .rotate(0)
        .font("Inria Sans")
        .on("end", draw)
        .start();

    function draw(words) {
    let maxsize = 30;
    let minsize = 18;
    // Use to remove word cloud one time in profile because $watch append so we get two svg element
    if(d3.select('.cloud').select('svg') != undefined){
      d3.select(".cloud").select('svg').remove();
    }
    d3.select(".cloud")
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate(" + (width/2).toString() + "," + (height/2).toString() + ")")
    .attr("width", width)
    .attr("height", height)
    .selectAll("text")
    .data(words)
    .enter()
    .append("text")
    .style("font-size", (d) => {
      /*if(minsize > d.size || d.size > maxsize){
      return (maxsize + Math.floor(Math.random() * 10) + Math.random()) + "px";
    }else{*/
    return (d.size + 3) + "px";

  })
  .style("font-family", "Roboto")
  .style("fill", (d, i) => {
    return fill[i%10];
  })
  .attr("text-anchor", "middle")
  .attr("transform", (d) => {
    return "translate(" + [(d.x).toString(), (d.y).toString()] + ")";
  })
  .attr("id", (d) => {
    return d.text;
  })
  .text((d) => {
    return d.text;
  })
  .on("click", (d) => {
    let url = "";
    let text = d.text;
    if(text.split(" ").length > 1){
        text = "\""+text+"\"";
    }
    let encodeWord = encodeURIComponent(d.text);
    if(d.affiliationId.split("-")[0] == "struct"){
      url = ("/publications-team-keyword/" + d.affiliationId + "/" + encodeWord);
    }else{
      url = ("/publications-individual-keyword/" + d.affiliationId + "/" + encodeWord);
    }
    window.open(encodeURI(url), "_blank");
  })
  .on("mouseover", (d) => {
    let color = d3.select("[id='" + d.text + "']").style("fill");
    for (let z in words) {
      d3.select("[id='" + words[z].text + "']").style("fill", "lightgray");
    }
    d3.select("[id='" + d.text + "']").style("fill", "red");
    let size = parseInt(d3.select("[id='" + d.text + "']").style("font-size").replace("px", ""));
    d3.select("[id='" + d.text + "']").style("font-size", (size + 7) + "px");
  })
  .on("mouseout", (d) => {
    let i = 0;
    let selected = d3.select("[id='" + d.text + "']");

    for (let z in words) {
      let word = words[z];
      d3.select("[id='" + word.text + "']").style("fill", function () {
        return fill[i%10];
      });
      i++;
    }

    selected.style("fill", color);
    let size = parseInt(selected.style("font-size").replace("px", ""));
    selected.style("font-size", (size - 7) + "px");
    selected = "";
    if (lastSelected != null) {
      lastSelected.style("fill", lastColor);
      let size = parseInt(lastSelected.style("font-size").replace("px", ""));
      lastSelected.style("font-size", (size - 7) + "px");
      lastSelected = null;
    }
  });

  d3.select("g").style("cursor", "pointer");
}
}
}
