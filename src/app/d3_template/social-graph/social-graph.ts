import {Component, Input, OnInit, Inject} from '@angular/core';
import * as d3 from 'd3';
import { SearchIndivService } from '../../services/es-indiv-serv';
import {UserUtils} from '../../utils/user.utils';
import {CleanWords} from '../../utils/cleanwords.utils';

import {DialogWordsCloud} from '../../template-page/dialog-words-cloud/dialog-words-cloud';
import {MatDialog} from '@angular/material';

/**
 * Use to diplay user relation in graph use d3 fxLayout
 * Only Element
 */

 @Component({
     selector : 'social-graph',
     template: "<div fxLayout='row' fxLayoutAlign='center center' id='graph'></div>"
 })

export class SocialGraph implements OnInit{
    @Input() origin;
    @Input() searchResp;

    searchService = new SearchIndivService();
    userUtils = new UserUtils();
    cleanWords = new CleanWords();

    constructor(private dialog : MatDialog){}

    ngOnInit(){
        this.createGraph();
    }

    createGraph(){
            let nodes = [];
            let links = [];

            let clickOrNot = [];
            let alreadyClick = [];

            let mapSearch = new Map();

            // The first search of similar user
            mapSearch.set(this.origin.userId, this.searchResp);

            // Initialise the graph with nodes and links
            let n = 0;
            nodes.push({
                name: this.origin.firstName + " " + this.origin.lastName,
                id: n,
                userId: this.origin.userId,
                color: "#e63312"
            });
            for (let i in this.searchResp.hits.hits) {
                let hit = this.searchResp.hits.hits[i];
                n = n + 1;
                nodes.push({
                    name: hit._source.forename + " " + hit._source.surname,
                    id: n,
                    userId: hit._id,
                    color: "#fff"
                });
                links.push({
                    source: 0,
                    target: n,
                    userId: hit._id,
                    firstName: hit._source.forename,
                    lastName: hit._source.surname
                });
            }
            updateWindow(this.searchService, this.cleanWords, this.userUtils, this.dialog, this.origin);

            // Use to create the graph
            function updateWindow(searchService, cleanWords, userUtils, dialog, origin) {

                d3.select("#graph").selectAll("*").remove();

                let width = d3.select("#graph").style("width").replace("px", "");
                let height = 800;
                //
                // if (width < "600") {
                //     width = "600";
                // }
                // if (height < "600") {
                //     height = "600";
                // }

                // let force = d3.fxLayout.force()
                //     .nodes(d3.values(nodes))
                //     .links(links)
                //     .size([width, height])
                //     .linkDistance(function(d){
                //         if(clickOrNot.includes(d.userId)){
                //             return 275;
                //         }else{
                //             return 150;
                //         }
                //     })
                //     .charge(-900)
                //     .on("tick", tick)
                //     .start();
                let is_done = false;
                let force = d3.forceSimulation(d3.values(nodes))
                                    .force("link", d3.forceLink(links).distance( (d) => {
                                        if(clickOrNot.indexOf(d.userId) != -1){
                                            return 200;
                        }else{
                            return 150;
                        }}
                ))
                .force("charge", d3.forceManyBody().strength(-900))
                .force("center", d3.forceCenter(+width / 2, +height / 2))
                .on("tick", tick).on('end', function(){is_done = true});
                //Select the element with id graph
                let svg = d3.select("#graph").append("svg")
                    .attr("width", width)
                    .attr("height", height)
                    .attr("fxLayoutAlign", "center")
                    .attr("id", "svg-graph");

                let defs = svg.insert("svg:defs").data(["end"]);

                // Select all the link and make them change with user action
                let link = svg.selectAll(".link")
                    .data(links)
                    .enter().append("line")
                    .attr("class", "link")
                    .on("mouseover", function () {
                        d3.select(this).style("stroke", "black").style("cursor", "pointer");
                    })
                    .on("mouseout", function () {
                        d3.select(this).style("stroke", "rgba(98, 98, 98, 0.45)");
                    })
                    .on("click", function (d) {
                        showCloud(d.source.userId, d.target.userId, searchService, cleanWords, userUtils, dialog);
                    });

                // let drag = force.drag()
                //     .on("dragstart", dragstart);
                //
                // function dragstart(d) {
                //     d3.select(this).classed("fixed", d.fixed = true);
                // }

                let node = svg.selectAll("g.node")
                    .data(force.nodes())
                    .enter().append("g")
                    .attr("class", "node")
                    .style("stroke", "black")
                    .style("stroke-width", "1px")
                    .on("mouseover", mouseover)
                    .on("mouseout", mouseout);
                    //.call(force.drag);
                    // .call(drag);

                // Write the name of the user under the circle
                node.append("text")
                    .attr("x", -35)
                    .attr("y", 40)
                    .style("font-size", "0.9em")
                    .style("pointer-events", "none")
                    .style("font-weight", "100")
                    .style("color", "#e63312")
                    .style("fill", "black !important")
                    .text(function (d) {
                        return d.name;
                    });

                // Create the size of the circle
                let clipPath = defs.append('clipPath')
                    .attr('id', 'clip-circle')
                    .append("circle")
                    .attr("r", 25);

                // Create big circle when user over the circle
                let clipPathLarge = defs.append('clipPath')
                    .attr('id', 'clip-circle-large')
                    .append("circle")
                    .attr("r", 35);

                // Create the circle
                node.append("circle")
                    .attr("r", 28)
                    .style("fill", function (d) {
                        return d.color;
                    })
                    .style("stroke", "black")
                    .style("stroke-width", "4px !important;");

                // Insert image in the circle and event when the user click on
                node.append("svg:image")
                    .attr("xlink:href", function (d) {
                        return "app/resources/images/profile.jpg";

                    })
                    .attr("x", function (d) {
                        return -25;
                    })
                    .attr("y", function (d) {
                        return -25;
                    })
                    .attr("height", 50)
                    .attr("width", 50)
                    .on("click", (d) => {
                        if (d3.event.defaultPrevented) return; // click suppressed
                        if(d3.event.detail > 1) return;
                        clickOrNot.push(d.userId);
                        // Get the similar users when the user click on a user
                        if(is_done){
                            searchService.getSimilarUsers(0, 4, d.userId).then(
                                resp => {
                                  if (nodes.length > 11) {
                                        var length_node = nodes.length;
                                        for (var i = 11; i < length_node; i++) {
                                            n = n - 1;
                                            nodes.splice(nodes.length - 1, 1);
                                            links.splice(links.length - 1, 1);
                                        }
                                    }
                                    for(let i = 0; i < resp.hits.hits.length;i++) {
                                        let hit = resp.hits.hits[i];
                                        if(origin.userId != hit._id){
                                          n = n + 1;
                                          nodes.push({
                                              name: hit._source.forename + " " + hit._source.surname,
                                              id: n,
                                              userId: hit._id,
                                              color: "#fff"
                                          });
                                          links.push({
                                              source: d.id,
                                              target: n,
                                              userId: hit._id,
                                              firstName: hit._source.forename,
                                              lastName: hit._source.surname
                                          });
                                        }
                                    }
                                    mapSearch.set(d.userId, resp);
                                    // Save the result of the user for word cloud
                                    // Use to way the graph to be finish
                            updateWindow(searchService, cleanWords, userUtils, dialog, origin)
                        })
                        }
                        })
                    .on("dblclick", function (d) {
                        // When double click open profile of the user
                        if (d3.event.defaultPrevented) return; // click suppressed
                        let url = ("/individual/" + d.userId);
                        window.open(url, "_blank");
                    })
                    .style("cursor", "pointer")
                    .style("border-radius", "50%")
                    .attr("clip-path", "url(#clip-circle)");

                // Use to place the graph
                function tick() {
                    link
                        .attr("x1", (d) =>{
                            return d.source.x;
                        })
                        .attr("y1", (d) =>{
                            return d.source.y;
                        })
                        .attr("x2", (d) =>{
                            return d.target.x;
                        })
                        .attr("y2", (d) =>{
                            return d.target.y;
                        });

                    node
                        .attr("transform", (d) =>{
                            return "translate(" + d.x + "," + d.y + ")";
                        });
                }

                // Change the size of the circle when user over
                function mouseover(d) {
                    d3.select(this).select("circle")
                        .attr("r", 38);

                    d3.select(this).select("image")
                        .attr("x", "-35")
                        .attr("y", "-35")
                        .attr("height", 70)
                        .attr("width", 70)
                        .attr("clip-path", "url(#clip-circle-large)");
                }

                // Change the size of the circle when user out
                function mouseout(d) {

                    d3.select(this).select("circle")
                        .attr("r", 28);

                    d3.select(this).select("image")
                        .attr("x", "-25")
                        .attr("y", "-25")
                        .attr("height", 50)
                        .attr("width", 50)
                        .attr("clip-path", "url(#clip-circle)");
                }
            }

            function showCloud(userIdSource, userIdTarget, searchService, cleanWords, userUtils, dialog) {
                let words = formattedUserSearchExplain(userIdTarget, userIdSource, searchService, cleanWords, userUtils, dialog);

            }

            /**
             * Use the word to format them for display the in the word cloud with different size (importance)
             * @param userId : user id
             * @returns {Array} : list of words format
             */
            function formattedUserSearchExplain(userId, userIdSource, searchService, cleanWords, userUtils, dialog) {
                searchService.getSimilarUsersExplain(userIdSource, userId).then(
                    resp => {
                        let explainJson = userUtils.affSearchExplain(userId, resp);
                        let resultwordtext = cleanWords.getCleanLevenshtein(explainJson);
                        let words = userUtils.normalizeScore(resultwordtext);
                        let msg = "Individuals Similar Keywords";
                        openDialog(words, msg, [], dialog);
            })
            }

            /**
             * Open a md Dialog with word cloud
             * @param bigrams : bigrams to show
             * @param msg : message to show one to of the dialog
             * @param words : words to show in the dialog
             */
            function openDialog(words, msg, bigrams, dialog){
                let dial = dialog.open(DialogWordsCloud, {
                    data : {msg : msg, words : words, bigrams : bigrams}
                });

            }
        }
    }
