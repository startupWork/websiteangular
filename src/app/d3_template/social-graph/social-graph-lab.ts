import {Component, Input, OnInit, Inject} from '@angular/core';
import * as d3 from 'd3';
import { SearchLabService } from '../../services/es-lab-serv';
import {UserUtils} from '../../utils/user.utils';
import {CleanWords} from '../../utils/cleanwords.utils';

import {DialogWordsCloud} from '../../template-page/dialog-words-cloud/dialog-words-cloud';
import {MatDialog} from '@angular/material';

/**
 * Use to diplay user relation in graph use d3 fxLayout
 * Only Element
 */

@Component({
    selector : 'social-graph-lab',
    template: "<div fxLayout='row' fxLayoutAlign='center center' id='graph'></div>"
})

export class SocialGraphLab implements OnInit{
        @Input() origin;
        @Input() searchResp;

        searchLabService = new SearchLabService();
        cleanWords = new CleanWords();
        userUtils = new UserUtils();

        constructor(private dialog : MatDialog){}

        ngOnInit(){
            this.createGraph();
        }

        createGraph(){
            let nodes = [];
            let links = [];

            let clickOrNot = [];
            let alreadyClick = [];

            let mapSearch = new Map();

            // The first search of similar user
            mapSearch.set(0, this.searchResp);

            // Initialise the graph with nodes and links
            let n = 0;
            nodes.push({
                name: this.origin.acronym,
                id: n,
                userId: this.origin.id,
                color: "#e63312"
            });
            for (let i in this.searchResp.hits.hits) {
                let hit = this.searchResp.hits.hits[i];
                n = n + 1;
                nodes.push({
                    name: hit._source.acronym,
                    id: n,
                    userId: hit._id,
                    color: "#fff"
                });
                links.push({
                    source: 0,
                    target: n,
                    userId: hit._id,
                    name: hit._source.name
                });
            }
            updateWindow(this.searchLabService, this.cleanWords, this.userUtils, this.dialog, this.origin);

            // Use to create the graph
            function updateWindow(searchLabService, cleanWords, userUtils, dialog, origin) {

                d3.select("#graph").selectAll("*").remove();

                let width = d3.select("#graph").style("width").replace("px", "");
                let height = 800;

                // if (width < "600") {
                //     width = "600";
                // }
                // if (height < "600") {
                //     height = "600";
                // }
                let is_done = false;

                let simulation = d3.forceSimulation(d3.values(nodes))
                    .force("link", d3.forceLink(links).distance( (d) => {
                        if(clickOrNot.indexOf(d.userId) != -1){
                            return 200;
                        }else{
                            return 150;
                        }}
                    ))
                    .force("charge", d3.forceManyBody().strength(-900))
                    .force("center", d3.forceCenter(+width / 2, +height / 2))
                    .on("tick", tick)
                    .on('end', function(){is_done = true});

                //Select the element with id graph
                let svg = d3.select("#graph").append("svg")
                .attr("width", width)
                .attr("height", height)
                    .attr("fxLayoutAlign", "center")
                    .attr("id", "svg-graph");

                let defs = svg.insert("svg:defs").data(["end"]);

                // Select all the link and make them change with user action
                let link = svg.selectAll(".link")
                    .data(links)
                    .enter().append("line")
                    .attr("class", "link")
                    .on("mouseover", function(){
                        d3.select(this).style("stroke", "black").style("cursor", "pointer");
                    })
                    .on("mouseout", function(){
                        d3.select(this).style("stroke", "rgba(98, 98, 98, 0.45)");
                    })
                    .on("click", d => {
                        openExplanation(d.source.userId, d.target.userId, searchLabService, cleanWords, userUtils, dialog, origin);
                    });

                // let drag = force
                //     .on("dragstart", dragstart;
                //
                // function dragstart(d) {
                //     d3.select(this).classed("fixed", d.fixed = true);
                // }

                let node = svg.selectAll("g.node")
                    .data(simulation.nodes())
                    .enter().append("g")
                    .attr("class", "node")
                    .on("mouseover", mouseover)
                    .on("mouseout", mouseout);
                    //.call(force.drag);
                    // .call(drag);

                // Write the name of the user under the circle
                node.append("text")
                    .attr("x", -35)
                    .attr("y", 40)
                    .style("font-size", "0.9em")
                    .style("pointer-events", "none")
                    .style("font-weight", "100")
                    .style("color", "#e63312")
                    .text( (d) => {
                        return d.name;
                    });

                // Create the size of the circle
                let clipPath = defs.append('clipPath')
                    .attr('id', 'clip-circle')
                    .append("circle")
                    .attr("r", 25);

                // Create big circle when user over the circle
                let clipPathLarge = defs.append('clipPath')
                    .attr('id', 'clip-circle-large')
                    .append("circle")
                    .attr("r", 35);

                // Create the circle
                node.append("circle")
                    .attr("r", 28)
                    .style("fill", function (d) {
                        let test = d as any;
                        return test.color;
                    })
                    .style("stroke", "black")
                    .style("stroke-width", "4px !important;");

                // Insert image in the circle and event when the user click on
                node.append("svg:image")
                    .attr("xlink:href", function (d) {
                        return "app/resources/images/affiliation.png";

                    })
                    .attr("x", function (d) {
                        return -25;
                    })
                    .attr("y", function (d) {
                        return -25;
                    })
                    .attr("height", 50)
                    .attr("width", 50)
                    .on("click", d => {
                        let data = d as any;
                        if (d3.event.defaultPrevented) return; // click suppressed
                        if(d3.event.detail > 1) return;
                        let userIdParam = data.userId;
                        clickOrNot.push(data.userId);
                        // Get the similar users when the user click on a user
                            if(is_done){
                            searchLabService.getSimilarAffiliation(0, 4, userIdParam).then(
                                resp => {
                                  if (nodes.length > 11) {
                                        var length_node = nodes.length;
                                        for (var i = 11; i < length_node; i++) {
                                            n = n - 1;
                                            nodes.splice(nodes.length - 1, 1);
                                            links.splice(links.length - 1, 1);
                                        }
                                    }
                                    for(let i=0; i< resp.hits.hits.length; i++){
                                            let hit = resp.hits.hits[i];
                                            if(hit._id != origin.id){
                                              n = n + 1;
                                              nodes.push({
                                                  name: hit._source.acronym,
                                                  id: n,
                                                  userId: hit._id,
                                                  color: "#fff"
                                              });
                                              links.push({
                                                  source: data.id,
                                                  target: n,
                                                  userId: hit._id,
                                                  name: hit._source.acronym
                                              });
                                            }
                                        }
                                    // Save the result of the user for word cloud
                                    mapSearch.set(data.id, resp);
                                    // Use to way the graph to be finish
                                    updateWindow(searchLabService, cleanWords, userUtils, dialog, origin);
                                })
                              }
                        }
                    )
                    .on("dblclick", function (d) {
                      let data = d as any;
                      // When double click open profile of the user
                      if (d3.event.defaultPrevented) return; // click suppressed
                      let url = ("/team/" + data.userId);
                      window.open(url, "_blank");
                    })
                    .style("cursor", "pointer")
                    .style("border-radius", "50%")
                    .attr("clip-path", "url(#clip-circle)");

                // Use to place the graph
                function tick() {
                    link
                        .attr("x1", (d) =>  {
                            return d.source.x;
                        })
                        .attr("y1", (d) =>  {
                            return d.source.y;
                        })
                        .attr("x2", (d) =>  {
                            return d.target.x;
                        })
                        .attr("y2", (d) =>  {
                            return d.target.y;
                        });

                    node
                        .attr("transform", (d) => {
                            return "translate(" + d.x + "," + d.y + ")";
                        });
                }

                // Change the size of the circle when user over
                function mouseover(d) {
                    d3.select(this).select("circle")
                        .attr("r", 38);

                    d3.select(this).select("image")
                        .attr("x", "-35")
                        .attr("y", "-35")
                        .attr("height", 70)
                        .attr("width", 70)
                        .attr("clip-path", "url(#clip-circle-large)");
                }

                // Change the size of the circle when user out
                function mouseout(d) {

                    d3.select(this).select("circle")
                        .attr("r", 28);

                    d3.select(this).select("image")
                        .attr("x", "-25")
                        .attr("y", "-25")
                        .attr("height", 50)
                        .attr("width", 50)
                        .attr("clip-path", "url(#clip-circle)");
                }
            }

            function openExplanation(affSimilar, affiliationId, searchLabService, cleanWords, userUtils, dialog, origin){
                let text = "";
                let main_topics = origin.main_topics;
              for(let main_topic of main_topics){
                text = text + main_topic;
              }
                searchLabService.getAffiliationsExplainBigram(affiliationId, text).then(
                    result => {
                        let explainJson = userUtils.affSearchExplainBigram(affiliationId, result);
                        //let words = UserUtils.userSearchExplainFormat(explainJson, affiliationId);
                        let cleanwords = cleanWords.getCleanLevenshtein(explainJson);
                        let words = userUtils.normalizeScore(cleanwords);
                        let msg = "Teams Similar Keywords";
                        openDialogCloud(msg, words, [], dialog);
                    })
            }

            /**
             * Open the dialog with the words get Keywords or Main topics
             * @param msg : message to show on top of dialog
             * @param words : words to display in the word cloud
             * @param bigram : bigram from eplain
             */
        function openDialogCloud(msg, words, bigram, dialog){
                let dial = dialog.open(DialogWordsCloud, {
                    data : {msg : msg, words : words, bigrams : bigram}
                });
        }
        }

    }
