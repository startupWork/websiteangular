import {Component, Input, OnInit} from '@angular/core';
import * as d3 from 'd3';
// Directive to display the graph of number of publications by years

@Component({
    selector : 'bar-chart',
    template: "<div id='bar-chart' fxfxLayoutAlign='center center'></div>"
})

export class BarChart implements OnInit{
    @Input() data;

    constructor(){}

    ngOnInit(){
        this.create_chart(this.data)
    }

    create_chart(loaddata) {
        let margin = {top: 20, right: 20, bottom: 70, left: 40},
            width = 500 - margin.left - margin.right,
            height = 400 - margin.top - margin.bottom;

        let wWidth = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;

        let wHeight = window.innerHeight
            || document.documentElement.clientHeight
            || document.body.clientHeight;

        if (wWidth < width) {
            width = wWidth;
        }
        if (wHeight < height) {
            height = wHeight;
        }
        //.rangeRoundBands([0, width], .05)

        // set the ranges
        var x = d3.scaleBand()
                  .range([0, width])
                  .padding(0.1);

        var y = d3.scaleLinear()
                  .range([height, 0]);

        let svg = d3.select("#bar-chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        let data = loaddata;



        let new_data = [];

        if(data.length > 20){
            data.forEach((value) => {
                if(value.key%10 != 0){
                    new_data.push({"key" : value.key.substring(2,4), "doc_count" : value.doc_count})
                }else{
                    new_data.push({"key" : value.key, "doc_count" : value.doc_count})
                }
        });
    }else if(data.length > 12){
        data.forEach((value) => {
            if(value.key%3 != 0){
                new_data.push({"key" : value.key.substring(2,4), "doc_count" : value.doc_count})
            }else{
                new_data.push({"key" : value.key, "doc_count" : value.doc_count})
            }
    });
    } else{
        data.forEach((value) => {
            new_data.push({"key" : value.key, "doc_count" : value.doc_count})
        })
        }

        let max = data.sort((a, b) => {
                    return b.doc_count - a.doc_count;
        });
        y.domain([0, max[0].doc_count]);

        x.domain(new_data.map( (d) => {
            return d.key;
        }));

        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

        svg.append("g")
            .call(d3.axisLeft(y));
        //.text("Publications number");

        svg.selectAll("bar")
            .data(new_data)
            .enter().append("rect")
            .style("fill", "rgb(230, 51, 18)")
            .attr("x", d => {
                let test = d as any;
                return x(test.key);
            })
            // .attr("width", x.rangeBand())
            .attr("y", (d) => {
                let test = d as any;
                return y(test.doc_count);
            })
            .attr("height", (d) => {
                let test = d as any;
                return height - y(test.doc_count);
            }).attr("width", x.bandwidth());

    }
}
