import {Component, Input, OnInit} from '@angular/core';
import * as d34 from 'd3';
import * as d3cloud from 'd3-cloud';
import * as d3dispatch from 'd3-dispatch';

let d3: any = d34;
/**
* Directive use to draw word cloud
*/

@Component({
  selector : 'word-cloud-dir',
  templateUrl: './word-cloud.html'
})
export class WordCloudDir implements OnInit{

  @Input() wordsModel;
  @Input() bigrams;
  @Input() msg;

  done = false;

  constructor(){}

  ngOnInit(){
    let fill = d3.schemeCategory10;;

    let selected, color;
    let lastSelected, lastColor, lastTextSelected = [];
    let width = window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth;

    let height = window.innerHeight
    || document.documentElement.clientHeight
    || document.body.clientHeight;

    if (width >= 600) {
      width = 600;
    }
    if (height >= 300) {
      height = 300;
    }
    if (width < 600) {
      width = width - 75;
    }
    if (height < 300) {
      height = height - 50;
    }
    if (width >= 700) {
      width = 700;
    }
    if (height >= 350) {
      height = 350;
    }
    if (width < 700) {
      width = width - 75;
    }
    if (height < 350) {
      height = height - 50;
    }
    let maxsize = 30;
    let minsize = 18;

    if(this.wordsModel.length == 1){
        this.wordsModel[0].size = 18;
    }

    d3cloud.size([width, height])
            .words(this.wordsModel)
            .padding(5)
            .rotate(0)
            .font("Inria Sans")
            .fontSize(function (d) {
                    return (d.size);
            })
            .on("end", draw)
            .start();
    this.done = true;

    function draw(words) {
    // Use to remove word cloud one time in profile because $watch append so we get two svg element
    if(d3.select('.cloudWords').select('svg') != undefined){
      d3.select(".cloudWords").select('svg').remove();
    }
    d3.select(".cloudWords")
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate(" + (width / 2).toString() + "," + (height / 2).toString() + ")")
    .attr("width", width)
    .attr("height", height)
    .selectAll("text")
    .data(words)
    .enter()
    .append("text")
    .style("font-size", function (d) {
      return (d.size + 3) + "px";
    })
    .style("font-family", "Roboto")
    .style("fill", function (d, i) {
      return fill[i%10];
    })
    .attr("text-anchor", "middle")
    .attr("transform", function (d) {
      return "translate(" + [(d.x).toString(), (d.y).toString()] + ")";
    })
    .attr("id", function (d) {
      return d.text;
    })
    .text(function (d) {
      return d.text;
    })
    .on("click", function (d) {
      let secondword = "";
      for(let i = 0; i < lastTextSelected.length; i++)
      secondword = secondword +" "+ lastTextSelected[i];
      // let encodeWord = encodeURIComponent(d.text + secondword);
      let url;
      let text = d.text;
      if(text.split(" ").length > 1 && !text.includes('"')){
          text = "\""+text+"\"";
      }
      let encodeWord = encodeURIComponent(text);
      if(!d.affiliationId.includes("struct-")){
        url = ("/publications-individual-keyword/" + d.userId + "/" + encodeWord);
      }else{
        url = ("/publications-team-keyword/" + d.affiliationId + "/" + encodeWord);
      }
      window.open(url, "_blank");
    })
    .on("mouseover",  (d) => {
      lastSelected = [];
      lastColor = [];
      lastTextSelected = [];
      let found = false;
      for (let i in this.bigrams) {
        let b = this.bigrams[i];
        if (b.indexOf(d.text) != -1) {
          let nb_found = 0;
          for (let z in words) {
            let word = words[z];
            if (word.text != d.text && b.indexOf(word.text) != -1) {
              lastColor.push(d3.select("[id='" + word.text + "']").style("fill"));
              lastSelected.push(d3.select("[id='" + word.text + "']"));
              lastTextSelected[nb_found] = word.text;
              let size = parseInt(lastSelected[nb_found].style("font-size").replace("px", ""));
              lastSelected[nb_found].style("font-size", (size + 7) + "px");
              nb_found = nb_found + 1;
              found = true;
            }
          }
          if (found) {
            break;
          }
        }
      }
      let selected = d3.select("[id='" + d.text + "']");
      color = selected.style("fill");
      for (let z in words) {
        d3.select("[id='" + words[z].text + "']").style("fill", "lightgray");
      }
      selected.style("fill", "red");
      if(found){
        for(let i=0; i < lastSelected.length; i++){
          lastSelected[i].style("fill", "red");
        }
      }
      let size = parseInt(selected.style("font-size").replace("px", ""));
      selected.style("font-size", (size + 7) + "px");
    })
    .on("mouseout",  (d) =>{
      let i = 0;
      for (let z in words) {
        let word = words[z];
        d3.select("[id='" + word.text + "']").style("fill", function () {
          return fill[i%10];
        });
        i++;
      }
      let selected = d3.select("[id='" + d.text + "']");
      let size = parseInt(selected.style("font-size").replace("px", ""));
      selected.style("font-size", (size - 7) + "px");
      selected = "";
      if (lastSelected != null && lastSelected != undefined) {
        for(let i=0; i < lastSelected.length; i++){
          lastSelected[i].style("fill", lastColor);
          let size = parseInt(lastSelected[i].style("font-size").replace("px", ""));
          lastSelected[i].style("font-size", (size - 7) + "px");
        }
      }
    });

    d3.select("g").style("cursor", "pointer");
    }


  }
}
