import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {SendMailServive} from '../../services/send_mail';

export interface DialogData {
  mail_send: boolean;
}

@Component({
    selector: 'send-results',
    templateUrl: "./send-results.html"
})

export class SendResults{

    email;


    constructor(public dialogRef: MatDialogRef<SendResults>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private send_mail: SendMailServive) {
        this.data.mail_send = true;
    }

    /**
    * Send email with data save by the user
    * @param email : eamil of the user
    */
    sendMailTo(email){
        let data = localStorage.getItem("data");
        let list_data = [];
        if (data == undefined) {
            list_data = [];
        } else {
            list_data = JSON.parse(data);
        }
        let users = "";
        let pubs = "";
        let teams = "";
        let your_search = [];
        let your_result = [];
        let your_query = "";
        let msg = "Hi, this is your basket items from LookinLabs!" + '\n' + '\n';
        for (let i=0; i < list_data.length; i++) {
            your_query = "";
            if(your_search.includes(list_data[i].search)){
                your_query = your_result[your_search.indexOf(list_data[i].search)];
            }else{
                your_search.push(list_data[i].search);
            }
            your_query = your_query + "" + list_data[i].name +": " +list_data[i].link+"\n";
            your_result[your_search.indexOf(list_data[i].search)] = your_query;
            // if (list_data[i].type == "individual") {
            //     users += "<a href='"+list_data[i].link+"'>" + list_data[i].name + "</a> (" + list_data[i].search + ')\n';
            // }else if(list_data[i].type == "team"){
            //     teams += "<a href='"+list_data[i].link+"'>" + list_data[i].name + "</a> (" + list_data[i].search + ')\n';
            // }
            // else {
            //     pubs += "<a href='"+list_data[i].link+"'>" + list_data[i].name + "</a> (" + list_data[i].search + ')\n';
            // }
        }
        for (let i=0; i < your_result.length; i++) {
            if(your_search[i] != "" && your_search[i] != undefined){
                msg = msg + "Your query : "+ your_search[i] + "\n\n" + your_result[i] + '\n'
            }else{
                msg = msg + "Other results save : \n\n" + your_result[i] + '\n'
            }
        }
        // if (users != "") {
        //     msg += "Individuals :" + '\n' + users + '\n';
        // }
        // if (pubs != "") {
        //     msg += "Publications :" + '\n' + pubs + '\n';
        // }
        // if(teams != ""){
        //     msg += "Teams :" + '\n' + teams + '\n';
        // }
        msg += "Thank you for using LookinLabs for your search.";
        msg = msg;

        /*let url = "lookinlabs-php/send-email.php?";*/
        // let url = "email=" + email;
        // url += "&comment=" + msg;

        this.send_mail.setRequest(email, list_data).then(
                resp => {
                    console.log(resp);
                },
                error => {
                    console.log(error);
                }
            );
        // this.http.post("https://alphadatascience.irisa.fr/send_mail", {
        //     method: "POST",
        //     email : email,
        //     // comment : msg,
        //     headers: { 'Content-Type': 'text/html; charset=utf-8', 'Cookie' : this.cookieService.get('list_save') }
        // }
        //
        //     // headers: { 'Content-Type': 'text/html' }
        // ).toPromise(
        // );

    }
}
