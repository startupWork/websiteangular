import {Component} from '@angular/core';
import {LocalStorageService} from '../../services/localStorageService';
import {MatDialog} from '@angular/material';
import {SendResults} from '../send-results/send-result';

@Component({
    selector : 'basket-results',
    templateUrl : './basket-results.html'
})
/**
 * Use to display the result in the basket
 * Result save by the user
 */

export class Basket{

    flexTeams = 30;
    flexIndividuals = 30;
    flexPublications = 40;

    teams = false;
    individuals = false;
    publications = false;

    teamsData = [];
    individualsData = [];
    publicationsData = [];
    isData = false;
    dialog_open_ok = false;
    dialog_open_ko = false;
    mail_send = false;

    constructor(private localStorageService : LocalStorageService, private dialog : MatDialog){
        let data = localStorage.getItem("data");
        let list_data = [];
        if (data == undefined || data == "undefined") {
            list_data = [];
        } else {
            list_data = JSON.parse(data);
        }

        for (let i =0; i < list_data.length; i++) {
            this.isData = true;
            if(list_data[i].type == "team"){
                this.teamsData.push(list_data[i]);
                this.teams = true;
            }else if(list_data[i].type == "individual"){
                this.individualsData.push(list_data[i]);
                this.individuals = true;
            }else if(list_data[i].type == "pub"){
                this.publicationsData.push(list_data[i]);
                this.publications = true;
            }
        }

        if (this.individuals && this.publications && this.teams) {
            this.flexIndividuals = 30;
            this.flexTeams = 30;
            this.flexPublications = 30;
        } else if (this.individuals && this.publications && !this.teams) {
            this.flexIndividuals = 45;
            this.flexTeams = 0;
            this.flexPublications = 45;
        } else if (this.individuals && !this.publications && this.teams){
            this.flexIndividuals = 45;
            this.flexTeams = 45;
            this.flexPublications = 0;
        } else if (!this.individuals && this.publications && this.teams){
            this.flexIndividuals = 0;
            this.flexTeams = 45;
            this.flexPublications = 45;
        } else if (this.individuals && !this.publications && !this.teams){
            this.flexIndividuals = 100;
            this.flexTeams = 0;
            this.flexPublications = 0;
        } else if (!this.individuals && this.publications && !this.teams){
            this.flexIndividuals = 0;
            this.flexTeams = 0;
            this.flexPublications = 100;
        } else if (!this.individuals && !this.publications && this.teams){
            this.flexIndividuals = 0;
            this.flexTeams = 100;
            this.flexPublications = 0;
        }
    }

    /**
    * When user click on button remove
    * @param  dataToRemove : data to remove from basket
    */
    remove(dataToRemove) {
        let data = localStorage.getItem("data");
        let list_data;
        if (data == undefined || data == "undefined") {
            list_data = [];
        } else {
            list_data = JSON.parse(data);
        }
        for (let i =0; i < list_data.length; i++) {
            if(list_data[i].id == dataToRemove.id){
                list_data.splice(i, 1);
                break
            }
        }
        if(list_data.length == 0){
            this.isData = false;
        }
        this.localStorageService.set(list_data);
        if(this.teamsData.indexOf(dataToRemove) != -1){
            this.teamsData.splice(this.teamsData.indexOf(dataToRemove), 1);
            if(this.teamsData.length == 0){
                this.teams = false;
            }
        }else if(this.individualsData.indexOf(dataToRemove) != -1){
            this.individualsData.splice(this.individualsData.indexOf(dataToRemove), 1);
            if(this.individualsData.length == 0){
                this.individuals = false;
            }
        }else if(this.publicationsData.indexOf(dataToRemove) != -1){
            this.publicationsData.splice(this.publicationsData.indexOf(dataToRemove), 1);
            if(this.publicationsData.length == 0){
                this.publications = false;
            }
        }

        if (this.individuals && this.publications && this.teams) {
            this.flexIndividuals = 30;
            this.flexTeams = 30;
            this.flexPublications = 30;
        } else if (this.individuals && this.publications && !this.teams) {
            this.flexIndividuals = 45;
            this.flexTeams = 0;
            this.flexPublications = 45;
        } else if (this.individuals && !this.publications && this.teams){
            this.flexIndividuals = 45;
            this.flexTeams = 45;
            this.flexPublications = 0;
        } else if (!this.individuals && this.publications && this.teams){
            this.flexIndividuals = 0;
            this.flexTeams = 45;
            this.flexPublications = 45;
        } else if (this.individuals && !this.publications && !this.teams){
            this.flexIndividuals = 100;
            this.flexTeams = 0;
            this.flexPublications = 0;
        } else if (!this.individuals && this.publications && !this.teams){
            this.flexIndividuals = 0;
            this.flexTeams = 0;
            this.flexPublications = 100;
        } else if (!this.individuals && !this.publications && this.teams){
            this.flexIndividuals = 0;
            this.flexTeams = 100;
            this.flexPublications = 0;
        }
    };

    /**
    * Open dialog for input email of user
    */
    showSendMail(){
        let dialogRef = this.dialog.open(SendResults, {
            data: {mail_send: this.mail_send,}
        });

    dialogRef.afterClosed().subscribe(result => {
        if(result != undefined){
            if(result){
                this.dialog_open_ok = true;
            }else{
                this.dialog_open_ko = true;
            }
        }
    });
    }
}
