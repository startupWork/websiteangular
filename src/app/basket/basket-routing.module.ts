import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';


import { Basket } from './basket-results/basket';

@NgModule({
  imports: [RouterModule.forChild([
    { path: '', component: Basket, pathMatch :'full'}
  ])],
  exports: [RouterModule]
})
export class BasketRoutingModule {}
