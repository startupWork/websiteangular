import { NgModule }  from '@angular/core';
import { CommonModule }  from '@angular/common';

import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";

import {BasketRoutingModule} from './basket-routing.module';
import { SharedModule }       from '../shared.module';


import {Basket} from './basket-results/basket';
import {SendResults} from './send-results/send-result';


import { WindowRefService } from '../services/windowRefService';
import {LocalStorageService} from '../services/localStorageService';

@NgModule({
  imports: [CommonModule, SharedModule, BasketRoutingModule, MaterialModule, FormsModule, FlexLayoutModule],
  declarations: [ SendResults, Basket],
  providers : [ WindowRefService, LocalStorageService],
})

export class BasketModule {}
