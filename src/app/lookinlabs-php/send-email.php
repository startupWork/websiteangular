<?php

// Email information
$admin_email = "lookinlabs@gmail.com";
$data = json_decode(file_get_contents('php://input'), true);
$email = $data['email'];
$subject = "Your basket from lookinlabs";
$comment = $data['comment'];

require 'PHPMailer-master/PHPMailerAutoload.php';
    if(strpos($comment, 'Hi, this is your basket items from LookinLabs!') !== false){
    $mail = new PHPMailer();

    $mail->isSMTP();                                            // Set mailer to use SMTP
    // $mail->Host = 'smtp.gmail.com';                          // Specify main and backup server
    //$mail->Host = 'smtp.gmail.com';
    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;                                     // Enable SMTP authentication
    $mail->Username = 'YOUR_MAIL';                   // SMTP username
    $mail->Password = 'YOUR_PASSWORD';                          // SMTP password
    $mail->SMTPSecure = 'tls';                               // Enable encryption, 'ssl' also accepted
    $mail->Port = 587;                                       // Set the SMTP port number - 587 for authenticated TLS
    $mail->setFrom($admin_email, "LookinLabs");                 // Set who the message is to be sent from
    // $mail->addReplyTo('labnol@gmail.com', 'First Last');     // Set an alternative reply-to address
    // $mail->addAddress('josh@example.net', 'Josh Adams');     // Add a recipient
    $mail->addAddress($email);                                  // Name is optional
    // $mail->addCC('cc@example.com');
    // $mail->addBCC('bcc@example.com');
    // $mail->WordWrap = 50;                                    // Set word wrap to 50 characters
    // $mail->addAttachment('/usr/labnol/file.doc');            // Add attachments
    // $mail->addAttachment('/images/image.jpg', 'new.jpg');    // Optional name
    // $mail->isHTML(true);                                     // Set email format to HTML

    $mail->Subject = $subject;
    $mail->Body = $comment;
    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    if (!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
        exit;
    }

    echo 'Message has been sent';
}
?>
