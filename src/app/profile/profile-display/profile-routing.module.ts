import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';


import { Profile } from './profile';

@NgModule({
  imports: [RouterModule.forChild([
    { path: ':userId', component: Profile}])],
  exports: [RouterModule]
})
export class ProfileRoutingModule {}
