import {Component, OnChanges, SimpleChange, OnInit, Inject, Input} from '@angular/core';
import {ClusteringService} from '../../services/clustering_service';

/*
*    Component to display publications in list
*/
@Component({
    selector : 'profile-clustering',
    templateUrl : './profile-clustering.html'
})

export class ProfileClustering implements OnInit{

    @Input() formatName;
    publications_lingo = {};
    words_lingo = [];
    done = false;

    constructor(private clusterService : ClusteringService){}

    ngOnInit(){
        this.getLingoTopics(this.formatName);
    }

    /**
    * Get cluster with Lingo algorithm
    * @param formatName : format name of the author
    **/
    private getLingoTopics(formatName){
      this.clusterService.getlingo_cluster(formatName).then(
        resp => {
          let res : any = resp;
          let label :any;
          for(label of res.clusters){
            this.publications_lingo[label.label] = label.documents
            this.words_lingo.push(label.label)
            this.done = true;
          }

        }
      )
    }

}
