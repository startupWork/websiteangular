// import { NgModule }  from '@angular/core';
// import { CommonModule }  from '@angular/common';
// import { MaterialModule } from '@angular/material';
// import { FormsModule } from '@angular/forms';
// import { FlexLayoutModule } from "@angular/flex-layout";
// // import { BrowserModule} from '@angular/platform-browser';
// // import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// // import { Angular2FontawesomeModule } from 'angular2-fontawesome';
//
// //template
// // import { Footer } from './template-page/footer/footer';
// import { Pagination } from './template-page/pagination/pagination';
//
// import {ResInfo } from './template-page/res-info/res-info';
// // import { ToolBarTop } from './template-page/toolbar-top/toolbar-top';
// import { PublicationAffDisplay, DialogAbstract } from './template-page/publication-display/publications-display';
// import { DialogWordsCloud } from './template-page/dialog-words-cloud/dialog-words-cloud';
//
// //d3
// import {WordCloudAff} from './d3_template/word-cloud-aff/word-cloud-aff';
// import {WordCloudDir} from './d3_template/word-cloud/word-cloud';
//
// //providers
// import {ElasticService} from './services/es-serv';
// import { WindowRefService } from './services/windowRefService';
// import {LocalStorageService} from './services/localStorageService';
//
// import  {ListResultsAff} from './affiliations/list-results-aff/list-result-lab';
// //
// //
// // //users
// // import { ProfileModule } from './profile/profile.module';
// import { ListResults } from './users/list-results/list-individuals';
//
// @NgModule({
//   imports: [CommonModule, MaterialModule, FormsModule, FlexLayoutModule],
//   declarations: [ Pagination, ListResultsAff, ListResults,
//                 PublicationAffDisplay,WordCloudAff, WordCloudDir, DialogAbstract, DialogWordsCloud, ResInfo],
//   entryComponents: [DialogAbstract, DialogWordsCloud],
//   exports : [ Pagination, ListResultsAff, ListResults, ResInfo,
//                 PublicationAffDisplay,WordCloudAff, WordCloudDir, DialogAbstract, DialogWordsCloud],
//   providers : [ ElasticService,
//       WindowRefService, LocalStorageService],
// })
//
// export class SharedModule {}
