import { Component, HostListener }  from '@angular/core';
import { MatomoInjector } from 'ngx-matomo';
// import { NgStyle } from '@angular/common';

 // min-height : 100%;
@Component({
  selector: 'my-app',
  template: `
  <div fxLayout="column" [ngStyle]="{'min-height.px': height}" style="width : 100%;">
    <cookies></cookies>
    <toolbar-top></toolbar-top>
        <div fxLayout="row" [ngStyle]="{'min-height.px': height_toolbar}" style="width : 100%;">
            <div class="frame binriagris" fxFlex="3" fxHide fxShow.gt-md="true"></div>
            <div fxFlex="100" fxFlex.gt-md="94">
                <router-outlet></router-outlet>
            </div>
            <div class="frame binriagris" fxFlex="3" fxHide fxShow.gt-md="true"></div>
        </div>
    <footer style="bottom: 0; width : 100%;"></footer>
    </div>
  `
})


export class AppComponent{
    height : number;
    height_toolbar : number;
    test = true;
    constructor(
    private matomoInjector: MatomoInjector
  ) {
        this.matomoInjector.init('//analysis.cominlabs.u-bretagneloire.fr/', 1);
        this.height = window.innerHeight
      || document.documentElement.clientHeight
      || document.body.clientHeight;
      this.height_toolbar = this.height - 64*2
  }

  @HostListener('window:beforeunload', ['$event'])
      unload(event) {
          let data = localStorage.getItem("cookies");
          if (data != undefined || data != "undefined") {
              if(data.substring(18,20) == 'ko'){
                  localStorage.clear();
                  // localStorage.remove("data");
              }
      }
  }

}
