# README #

Website update with Angular version 4

### What is this repository for? ###

Website lookinlabs

### build the aot website ###

You need nodes.js (https://nodejs.org/en/)


run ./compile.sh
copy the /aot folder in your server


For apache use this config (replace * by your config)

```xml
<VirtualHost _default_:443>
    Header *
    DocumentRoot "path to your website"
    ServerName *
    ServerAlias *
    SSLEngine On
    SSLCertificateFile *
    SSLCertificateKeyFile *
    SSLCertificateChainFile *
    <Directory "path to your website">
        Options Indexes FollowSymLinks MultiViews
        AllowOverride None
        Require all granted
        RewriteEngine On
        RewriteCond "%{DOCUMENT_ROOT}%{REQUEST_URI}" -f [OR]
        RewriteCond "%{DOCUMENT_ROOT}%{REQUEST_URI}" -d
        RewriteRule "^" "-" [L]
        RewriteRule "^" "index.html"
    </Directory>
</VirtualHost>
```
