/**
 * Test for publication
 */
describe('Test', function () {

    describe('Publication', function() {
        beforeEach(function () {
            browser.get('/');
            var textSearch = element(by.id("mat-input-0"));
            textSearch.clear();
            textSearch.sendKeys('Test Synthesis from UML Models of Distributed Software  av');
            browser.sleep(200);
            element.all(by.id('button-search')).click();
            browser.sleep(200);
        });

        it('Should render similars publications in a new tab', function () {
            element.all(by.id('go-to-similar-pub')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/publications-similars/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Go to hal link', function () {
            element.all(by.id('link-hal')).get(1).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("https://hal.inria.fr/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Show abstarct', function () {
            element.all(by.id('publication-display')).get(0).click();
            expect(element(by.tagName('md-dialog-container'))).toBeDefined();
        });

        it('Should update basket with 1 publication', function () {
            element.all(by.tagName('mat-checkbox')).get(1).click();
            element.all(by.id('save-pub')).get(0).click();
            var nbsave = element.all(by.id('save-nb')).get(0).getText();
            expect(nbsave).toEqual('2');
        });

    });

});
