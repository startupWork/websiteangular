'use strict';
/**
 * Created by jhany on 11/21/16.
 */

describe('Test Team', function () {

    describe('With topic markov model', function () {
            beforeEach(function () {
            browser.get('');
            var textSearch = element(by.id("mat-input-0"));
            textSearch.clear();
            textSearch.sendKeys('markov model');
            browser.sleep(200);
            element.all(by.id('button-search')).click();
            browser.sleep(200);
        });

        it('Should open a new tab profile', function () {
            element.all(by.id('go-to-profile-aff')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/team/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Go to publication team', function(){
            element.all(by.id('publication_desc')).get(2).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/publications-team-keyword/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Should open a dialog', function () {
            element.all(by.id('btn-aff-explain')).get(0).click();
            expect(element(by.tagName('word-cloud-dir'))).toBeDefined();
        });

        it('Should go to people with keywords', function () {
            element.all(by.id('btn-aff-people')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/individuals-team-keyword/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Go to similar team', function () {
            element.all(by.id('btn-aff-similar')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/teams-similar/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Should update basket with publication', function () {
            element.all(by.className('list-btn-shop')).get(0).click();
            var nbsave = element.all(by.id('save-nb')).get(0).getText();
            expect(nbsave).toEqual('3');
        });


    });

    describe('Team display Domain', function () {
        beforeEach(function () {
            browser.get('/team/struct-178918');
        });

        it('More topics', function () {
            var el = element.all(by.id('more_words')).get(0);
            el.click();
            expect(el.getAttribute('mattooltip')).toBe("Show less topics")
        });

        it('More Keywords', function () {
            var el = element.all(by.id('more_words')).get(1);
            el.click();
            expect(el.getAttribute('mattooltip')).toBe("Show less topics")
        });

        it('Bar chart publication', function () {
            expect(element(by.tagName('bar-chart'))).toBeDefined();
        });

        it('Publication Keywords', function () {
            var el = element.all(by.tagName('text')).get(1).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/publications-team-keyword/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Should update basket with publication', function () {
            element.all(by.id('save-pub')).get(0).click();
            var nbsave = element.all(by.id('save-nb')).get(0).getText();
            expect(nbsave).toEqual('4');
        });

        it('should go to similar publication', function () {
            element.all(by.id('go-to-similar-pub')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/publications-similars/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

    });

        describe('Team People', function () {
            beforeEach(function () {
                browser.get('/team/struct-178918');
            });

            it('should go to people affiliation', function () {
                element.all(by.id('mat-tab-label-0-1')).get(0).click();
                var goToProfile = element.all(by.id('go-to-profile-aff'));
                expect(goToProfile).toBeDefined();
                expect(browser.getCurrentUrl()).toMatch("/team/struct-178918");
            });

            it('should go to a people profile', function () {
                element.all(by.id('mat-tab-label-0-1')).get(0).click();
                browser.sleep(600);
                element.all(by.id('go-to-profile-user')).get(5).click();
                browser.getAllWindowHandles().then(function (handles) {
                    browser.ignoreSynchronization = true;
                    browser.sleep(600);
                    browser.switchTo().window(handles[1]);
                    expect(browser.getCurrentUrl()).toMatch("/individual/*");
                    browser.ignoreSynchronization = false;
                    browser.close();
                    browser.switchTo().window(handles[0]);
                });
            });

            it('should go to similar people', function () {
                element.all(by.id('mat-tab-label-0-1')).get(0).click();
                browser.sleep(600);
                element.all(by.id('button-similars')).get(9).click();
                browser.getAllWindowHandles().then(function (handles) {
                    browser.ignoreSynchronization = true;
                    browser.sleep(600);
                    browser.switchTo().window(handles[1]);
                    expect(browser.getCurrentUrl()).toMatch("/individuals-similars/*");
                    var goToProfile = element.all(by.className('go-to-profile-user'));
                    browser.ignoreSynchronization = false;
                    expect(goToProfile).toBeDefined();
                    browser.close();
                    browser.switchTo().window(handles[0]);
                });
            });

            it('should go to graph similar people', function () {
                element.all(by.id('mat-tab-label-0-1')).get(0).click();
                browser.sleep(600);
                element.all(by.id('button-people')).get(9).click();
                browser.getAllWindowHandles().then(function (handles) {
                    browser.ignoreSynchronization = true;
                    browser.sleep(600);
                    browser.switchTo().window(handles[1]);
                    expect(browser.getCurrentUrl()).toMatch("/individuals-relations/*");
                    browser.ignoreSynchronization = false;
                    var goToProfile = element.all(by.className('go-to-profile-user'));
                    expect(goToProfile).toBeDefined();
                    browser.close();
                    browser.switchTo().window(handles[0]);
                });
            });

        });

        describe('People in team with Keywords', function () {
            beforeEach(function () {
                browser.get('/individuals-team-keyword/struct-2539/test');
                browser.sleep(1000);
            });

            it('should go to similar people', function () {
                element.all(by.id('button-similars')).get(9).click();
                browser.getAllWindowHandles().then(function (handles) {
                    browser.ignoreSynchronization = true;
                    browser.sleep(600);
                    browser.switchTo().window(handles[1]);
                    expect(browser.getCurrentUrl()).toMatch("/individuals-similars/*");
                    browser.ignoreSynchronization = false;
                    var goToProfile = element.all(by.className('go-to-profile-user'));
                    expect(goToProfile).toBeDefined();
                    browser.close();
                    browser.switchTo().window(handles[0]);
                });
            });

            it('should go to graph similar people', function () {
                element.all(by.id('button-people')).get(9).click();
                browser.getAllWindowHandles().then(function (handles) {
                    browser.ignoreSynchronization = true;
                    browser.sleep(600);
                    browser.switchTo().window(handles[1]);
                    expect(browser.getCurrentUrl()).toMatch("/individuals-relations/*");
                    browser.ignoreSynchronization = false;
                    var goToProfile = element.all(by.className('go-to-profile-user'));
                    expect(goToProfile).toBeDefined();
                    browser.close();
                    browser.switchTo().window(handles[0]);
                });
            });

            it('should go to a people profile', function () {
                element.all(by.id('go-to-profile-user')).get(9).click();
                browser.getAllWindowHandles().then(function (handles) {
                    browser.ignoreSynchronization = true;
                    browser.sleep(600);
                    browser.switchTo().window(handles[1]);
                    expect(browser.getCurrentUrl()).toMatch("/individual/*");
                    browser.ignoreSynchronization = false;
                    browser.close();
                    browser.switchTo().window(handles[0]);
                });
            });

        });
});
