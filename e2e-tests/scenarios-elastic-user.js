'use strict';
/**
 * Created by jhany on 11/21/16.
 */

describe('Test', function () {

    describe('User', function() {
        beforeEach(function () {
            browser.get('');
            var textSearch = element(by.id("mat-input-0"));
            textSearch.clear();
            textSearch.sendKeys('cloud computing');
            browser.sleep(200);
            element.all(by.id('button-search')).click();
            browser.sleep(200);
        });

        it('Should render similars users in a new tab', function () {
            element.all(by.id('button-similars')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.sleep(1000);
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/individuals-similars/*");
                browser.ignoreSynchronization = false;
                var nbUser = element.all(by.id('list-card'));
                expect(nbUser.count()).toBeGreaterThan(1);
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Should render publication of user keywords', function () {
            var nbPub = element.all(by.id('nb-publication-keywords')).get(0);
            var nbPubText = nbPub.getText();
            element.all(by.id('publication-keywords')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.sleep(1000);
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/publications-individual-keyword/*/*");
                browser.ignoreSynchronization = false;
                var pubText = element(by.id('total-hit')).getText();
                expect(pubText).toEqual(nbPubText);
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Should open a new tab user relations', function () {
            element.all(by.id('button-people')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.sleep(400);
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/individuals-relations/*");
                browser.ignoreSynchronization = false;
                expect(element(by.tagName('svg'))).toBeDefined();
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Should open a new tab profile', function () {
            element.all(by.id('go-to-profile-user')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/individual/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Should open a dialog explanation', function () {
            element.all(by.id('btn-user-why')).get(0).click();
            expect(element(by.tagName('word-cloud-dir'))).toBeDefined();
        });

        it('Should update basket to 1', function () {
            element.all(by.id('save-user')).get(0).click();
            var nbsave = element.all(by.id('save-nb')).get(0).getText();
            expect(nbsave).toEqual('1');
        });

        it('Should go to basket and see 1 item', function () {
            element.all(by.id('basket')).get(0).click();
            browser.sleep(200);
            var list = element.all(by.className('mat-list-item'));
            expect(list.count()).toBe(1);
        });

    });

    describe('People', function () {
      browser.get('');
      var textSearch = element(by.id("mat-input-0"));
      textSearch.clear();
      textSearch.sendKeys('cloud computing');
      element.all(by.id('button-search')).click();
      browser.sleep(200);
      var user_id = "";
      var new_el = element.all(by.id('go-to-profile-user')).get(1).getAttribute('href').then(function(hrefValue){
        user_id = hrefValue;
      });

        beforeEach(function () {
            browser.get(user_id);
        });

        it('More topics', function () {
            var el = element.all(by.id('more_words')).get(0);
            el.click();
            expect(el.getAttribute('mattooltip')).toBe("Show less topics")
        });

        it('More Keywords', function () {
            var el = element.all(by.id('more_words')).get(1);
            el.click();
            expect(el.getAttribute('mattooltip')).toBe("Show less topics")
        });

        it('Bar chart publication', function () {
            expect(element(by.tagName('bar-chart'))).toBeDefined();
        });

        it('Publication Keywords', function () {
            var el = element.all(by.tagName('text')).get(1).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/publications-individual-keyword/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('should go to similar publication', function () {
            element.all(by.id('go-to-similar-pub')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/publications-similars/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });

        it('Go to team', function () {
            element.all(by.className('address-aff')).get(0).click();
            browser.getAllWindowHandles().then(function (handles) {
                browser.ignoreSynchronization = true;
                browser.switchTo().window(handles[1]);
                expect(browser.getCurrentUrl()).toMatch("/team/*");
                browser.ignoreSynchronization = false;
                browser.close();
                browser.switchTo().window(handles[0]);
            });
        });
    });

});
